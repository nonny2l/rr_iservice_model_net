namespace RRPlatFormModel
{
    using RRPlatFormModel.Models;
    using System.Data.Entity;

    public partial class RRIServiceModelContext : DbContext
    {
        public RRIServiceModelContext()
          : base("name=RRIServiceModel")
        {

        }
        public DbSet<LOG_API> LOG_API { get; set; }

        //User
        public DbSet<MT_USER> MT_USER { get; set; }
        public virtual DbSet<MT_USER_ROLE> MT_USER_ROLE { get; set; }
        public virtual DbSet<MT_USER_TYPE> MT_USER_TYPE { get; set; }
        public virtual DbSet<MT_USER_GROUP> MT_USER_GROUP { get; set; }
        public virtual DbSet<MT_USER_GROUP_ROLE> MT_USER_GROUP_ROLE { get; set; }

        //Menu
        public virtual DbSet<MT_MENU> MT_MENU { get; set; }
        public virtual DbSet<MT_MENU_TYPE> MT_MENU_TYPE { get; set; }
        public virtual DbSet<MT_MENU_GROUP> MT_MENU_GROUP { get; set; }
        public virtual DbSet<MT_MENU_GROUP_PERMISSION> MT_MENU_GROUP_PERMISSION { get; set; }
        public virtual DbSet<MT_USER_MENU_PERMISSION> MT_USER_MENU_PERMISSION { get; set; }
        public virtual DbSet<MT_MENU_ROLE_PERMISSION> MT_MENU_ROLE_PERMISSION { get; set; }
        public virtual DbSet<MT_MENU_TYPE_PERMISSION> MT_MENU_TYPE_PERMISSION { get; set; }

        // Master Config H,D
        public virtual DbSet<MT_CONFIG_H> MT_CONFIG_H { get; set; }
        public virtual DbSet<MT_CONFIG_D> MT_CONFIG_D { get; set; }

        ///  PROVINCE, DISTRICT, SUB DISTRICT
        public virtual DbSet<MT_PROVINCE> MT_PROVINCE { get; set; }
        public virtual DbSet<MT_DISTRICT> MT_DISTRICT { get; set; }
        public virtual DbSet<MT_SUBDISTRICT> MT_SUBDISTRICT { get; set; }

        //TR COMPANY CUSTOMER
        public virtual DbSet<MT_COMPANY> MT_COMPANY { get; set; }
        public virtual DbSet<TR_COMPANY_ADDRESS> TR_COMPANY_ADDRESS { get; set; }
        public virtual DbSet<TR_COMPANY_PERSON> TR_COMPANY_PERSON { get; set; }
        public virtual DbSet<TR_COMPANY_HEAD_CS> TR_COMPANY_HEAD_CS { get; set; }
        public virtual DbSet<MT_PERSON> MT_PERSON { get; set; }
        public virtual DbSet<MT_ORG_POSITION> MT_ORG_POSITION { get; set; }

        //Service
        public virtual DbSet<MT_SERVICE> MT_SERVICE { get; set; }
        public virtual DbSet<MT_SUB_SERVICE> MT_SUB_SERVICE { get; set; }

        //Employee
        public virtual DbSet<MT_EMPLOYEE> MT_EMPLOYEE { get; set; }
        public virtual DbSet<MT_EMPLOYEE_POSITION> MT_EMPLOYEE_POSITION { get; set; }
        public virtual DbSet<MT_TEAM_ROLE> MT_TEAM_ROLE { get; set; }

        //Department
        public virtual DbSet<MT_DEPARTMENT> MT_DEPARTMENT { get; set; }

        //position
        public virtual DbSet<MT_POSITION> MT_POSITION { get; set; }

        //project
        public virtual DbSet<MT_PROJECT> MT_PROJECT { get; set; }
        public virtual DbSet<TR_JOB> TR_JOB { get; set; }

        //product type
        public virtual DbSet<MT_PRODUCT_TYPE> MT_PRODUCT_TYPE { get; set; }

        //task 
        public virtual DbSet<TR_TASK> TR_TASK { get; set; }
        public virtual DbSet<TR_ASSIGN_TO> TR_ASSIGN_TO { get; set; }
        public virtual DbSet<TR_JOB_TASK> TR_JOB_TASK { get; set; }
        public virtual DbSet<TR_TIME_SHEET> TR_TIME_SHEET { get; set; }
        public virtual DbSet<TR_TASK_FOLLOW> TR_TASK_FOLLOW { get; set; }
        public virtual DbSet<MT_STATUS> MT_STATUS { get; set; }
        public virtual DbSet<MT_TASK_GROUP> MT_TASK_GROUP { get; set; }
        //public virtual DbSet<MT_TASK_STATUS> MT_TASK_STATUS { get; set; }
        //public virtual DbSet<TR_TASK_DOCUMENT> TR_TASK_DOCUMENT { get; set; }
        //public virtual DbSet<TR_TASK_DOCUMENT_COMMENT> TR_TASK_DOCUMENT_COMMENT { get; set; }
        public virtual DbSet<TR_TEAM> TR_TEAM { get; set; }
        public virtual DbSet<TR_TEAM_ASSIGN> TR_TEAM_ASSIGN { get; set; }
        public virtual DbSet<TR_JOB_ATTACHMENT> TR_JOB_ATTACHMENT { get; set; }




        public virtual DbSet<MT_ESTIMATED_TIME> MT_ESTIMATED_TIME { get; set; }
        public virtual DbSet<MT_PRIORITY> MT_PRIORITY { get; set; }
        public virtual DbSet<TR_DOC_RUNNING> TR_DOC_RUNNING { get; set; }
        public virtual DbSet<MT_JOB_STATUS> MT_JOB_STATUS { get; set; }
        public virtual DbSet<MT_BUDGET_UNIT> MT_BUDGET_UNIT { get; set; }



        
        public virtual DbSet<MT_NOTIFY_CONFIG> MT_NOTIFY_CONFIG { get; set; }
        public virtual DbSet<MT_NOTIFY_TOPIC> MT_NOTIFY_TOPIC { get; set; }
        //public virtual DbSet<MT_NOTIFY_GROUP> MT_NOTIFY_GROUP { get; set; }
        //public virtual DbSet<TR_NOTIFY_GROUP_ROLE> TR_NOTIFY_GROUP_ROLE { get; set; }
        //public virtual DbSet<TR_NOTIFY_USER_ROLE> TR_NOTIFY_USER_ROLE { get; set; }
        public virtual DbSet<TR_NOTIFY_MSG> TR_NOTIFY_MSG { get; set; }
        public virtual DbSet<TR_NOTIFY_READ> TR_NOTIFY_READ { get; set; }


        //public virtual DbSet<TR_COMPANY_CA> TR_COMPANY_CA { get; set; }
        //public virtual DbSet<TR_COMPANY_CA_STATUS_HISTORY> TR_COMPANY_CA_STATUS_HISTORY { get; set; }
        //public virtual DbSet<TR_CA_URL> TR_CA_URL { get; set; }
        //public virtual DbSet<TR_COMPANY_REGISTER> TR_COMPANY_REGISTER { get; set; }
        //public virtual DbSet<TR_REGISTER_ADDRESS> TR_REGISTER_ADDRESS { get; set; }

        // TR DOCUMENT TR_ACTIVITY
        //public virtual DbSet<TR_ACTIVITY> TR_ACTIVITY { get; set; }
        //public virtual DbSet<TR_DOCUMENT_H> TR_DOCUMENT_H { get; set; }
        //public virtual DbSet<TR_DOCUMENT_D> TR_DOCUMENT_D { get; set; }




        //public virtual DbSet<TR_STAFF_TEAM> TR_STAFF_TEAM { get; set; }


        //public DbSet<ACTIVITY_CUSTOMER_HISTORY> ACTIVITY_CUSTOMER_HISTORY { get; set; }
        //public DbSet<CUSTOMER_PROFILE> CUSTOMER_PROFILE { get; set; }
        //public DbSet<EDM_D> EDM_D { get; set; }
        //public DbSet<EDM_H> EDM_H { get; set; }
        //public DbSet<LIST_GOOGLE_FORM> LIST_GOOGLE_FORM { get; set; }
        //public DbSet<MT_CUSTOMER_STATUS> MT_CUSTOMER_STATUS { get; set; }
        //public DbSet<MT_EDM_TYPE> MT_EDM_TYPE { get; set; }
        //public DbSet<SOURCE_DATA> SOURCE_DATA { get; set; }
        //public DbSet<TR_APPOINTMENT> TR_APPOINTMENT { get; set; }
        //public DbSet<CONTRACT_LIST> CONTRACT_LIST { get; set; }
        //public DbSet<TR_WEB_HOOK_HISTORY> TR_WEB_HOOK_HISTORY { get; set; }

        //public virtual DbSet<MT_QUESTION_TYPE> MT_QUESTION_TYPE { get; set; }
        //public virtual DbSet<MT_QUESTION> MT_QUESTION { get; set; }
        //public virtual DbSet<MT_ANSWER> MT_ANSWER { get; set; }


        //public virtual DbSet<MT_VENDOR> MT_VENDOR { get; set; }


        //TR CALL
        //public virtual DbSet<TR_CALL_H> TR_CALL_H { get; set; }
        //public virtual DbSet<TR_CALL_D> TR_CALL_D { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new DecimalPrecisionAttributeConvention());

            //modelBuilder.HasDefaultSchema("dbo");
            base.OnModelCreating(modelBuilder);
        }
    }
}
