﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_TEAMandTR_TEAM_ROLE : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_TEAM",
                c => new
                    {
                        TEAM_ID = c.Int(nullable: false, identity: true),
                        TEAM_CODE = c.String(maxLength: 100),
                        DEPT_CODE = c.String(maxLength: 3),
                        JOB_NO = c.String(maxLength: 20),
                        TEAM_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TEAM_ID);
            
            CreateTable(
                "dbo.TR_TEAM_ROLE",
                c => new
                    {
                        TEAM_ROLE_ID = c.Int(nullable: false, identity: true),
                        TEAM_CODE = c.String(maxLength: 100),
                        EMP_NO = c.String(maxLength: 3),
                        IS_MAIN = c.String(maxLength: 1),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TEAM_ROLE_ID);
            
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_CODE", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_CODE", c => c.String(maxLength: 20));
            DropTable("dbo.TR_TEAM_ROLE");
            DropTable("dbo.TR_TEAM");
        }
    }
}
