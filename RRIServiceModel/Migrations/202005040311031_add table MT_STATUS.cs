﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableMT_STATUS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_STATUS",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        PROJECT_CODE = c.String(maxLength: 100),
                        STATUS_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_STATUS");
        }
    }
}
