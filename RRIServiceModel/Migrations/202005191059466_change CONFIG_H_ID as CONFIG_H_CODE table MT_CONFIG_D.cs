﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeCONFIG_H_IDasCONFIG_H_CODEtableMT_CONFIG_D : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MT_CONFIG_D");
            AddColumn("dbo.MT_CONFIG_D", "CONFIG_H_CODE", c => c.String(nullable: false, maxLength: 20));
            AddPrimaryKey("dbo.MT_CONFIG_D", new[] { "CONFIG_D_ID", "CONFIG_H_CODE" });
            DropColumn("dbo.MT_CONFIG_D", "CONFIG_H_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MT_CONFIG_D", "CONFIG_H_ID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.MT_CONFIG_D");
            DropColumn("dbo.MT_CONFIG_D", "CONFIG_H_CODE");
            AddPrimaryKey("dbo.MT_CONFIG_D", new[] { "CONFIG_D_ID", "CONFIG_H_ID" });
        }
    }
}
