﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcollumnTASK_JOB_NAMETABLETR_JOB_TASK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB_TASK", "TASK_JOB_NAME", c => c.String(maxLength: 255));
            AlterColumn("dbo.TR_JOB_TASK", "JOB_NO", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TR_JOB_TASK", "JOB_NO", c => c.String(maxLength: 20));
            DropColumn("dbo.TR_JOB_TASK", "TASK_JOB_NAME");
        }
    }
}
