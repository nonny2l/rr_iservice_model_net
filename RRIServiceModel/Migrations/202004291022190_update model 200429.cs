﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatemodel200429 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MT_POSITION");
            DropPrimaryKey("dbo.MT_PROJECT_D");
            DropPrimaryKey("dbo.MT_SERVICE");
            DropPrimaryKey("dbo.MT_SUB_SERVICE");
            AddColumn("dbo.MT_COMPANY", "AGENCY_FEE", c => c.Int());
            AddColumn("dbo.MT_EMPLOYEE", "POSITION_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_POSITION", "POSITION_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_POSITION", "POSITION_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_POSITION", "POSITION_NAME", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_PROJECT_D", "SERVICE_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_PROJECT_D", "SUB_SERVICE_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_PROJECT_D", "ACTUAL_START_DATE", c => c.DateTime());
            AddColumn("dbo.MT_PROJECT_D", "ACTUAL_FINISH_DATE", c => c.DateTime());
            AddColumn("dbo.MT_SERVICE", "SERVICE_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_SERVICE", "SERVICE_NAME", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.MT_SUB_SERVICE", "SUB_SERVICE_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_SUB_SERVICE", "SERVICE_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_SUB_SERVICE", "SUB_SERVICE_NAME", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.TR_COMPANY_PERSON", "CONTACT_TYPE_CODE", c => c.String(maxLength: 60));
            AddColumn("dbo.TR_COMPANY_PERSON", "PERSON_SOURCE_CODE", c => c.String(maxLength: 60));
            AddPrimaryKey("dbo.MT_POSITION", new[] { "POSITION_ID", "POSITION_CODE" });
            AddPrimaryKey("dbo.MT_PROJECT_D", new[] { "PROJECT_D_ID", "PROJECT_H_ID", "SERVICE_ID", "SUB_SERVICE_ID", "PROD_TYPE_CODE" });
            AddPrimaryKey("dbo.MT_SERVICE", "SERVICE_ID");
            AddPrimaryKey("dbo.MT_SUB_SERVICE", new[] { "SUB_SERVICE_ID", "SERVICE_ID" });
            DropColumn("dbo.MT_COMPANY", "AGENCY_FREE");
            DropColumn("dbo.MT_EMPLOYEE", "POSN_CODE");
            DropColumn("dbo.MT_POSITION", "POSN_ID");
            DropColumn("dbo.MT_POSITION", "POSN_CODE");
            DropColumn("dbo.MT_POSITION", "POSN_NAME");
            DropColumn("dbo.MT_PROJECT_D", "SRVC_ID");
            DropColumn("dbo.MT_PROJECT_D", "SUB_SRVC_ID");
            DropColumn("dbo.MT_PROJECT_D", "ACTL_START_DATE");
            DropColumn("dbo.MT_PROJECT_D", "ACTL_FINISH_DATE");
            DropColumn("dbo.MT_SERVICE", "SRVC_ID");
            DropColumn("dbo.MT_SERVICE", "SRVC_NAME");
            DropColumn("dbo.MT_SUB_SERVICE", "SUB_SRVC_ID");
            DropColumn("dbo.MT_SUB_SERVICE", "SRVC_ID");
            DropColumn("dbo.MT_SUB_SERVICE", "SUB_SRVC_NAME");
            DropColumn("dbo.TR_COMPANY_PERSON", "CONTACT_TYPE_CFCODE");
            DropColumn("dbo.TR_COMPANY_PERSON", "PERSON_SOURCE_CFCODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_COMPANY_PERSON", "PERSON_SOURCE_CFCODE", c => c.String(maxLength: 60));
            AddColumn("dbo.TR_COMPANY_PERSON", "CONTACT_TYPE_CFCODE", c => c.String(maxLength: 60));
            AddColumn("dbo.MT_SUB_SERVICE", "SUB_SRVC_NAME", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.MT_SUB_SERVICE", "SRVC_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_SUB_SERVICE", "SUB_SRVC_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_SERVICE", "SRVC_NAME", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.MT_SERVICE", "SRVC_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_PROJECT_D", "ACTL_FINISH_DATE", c => c.DateTime());
            AddColumn("dbo.MT_PROJECT_D", "ACTL_START_DATE", c => c.DateTime());
            AddColumn("dbo.MT_PROJECT_D", "SUB_SRVC_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_PROJECT_D", "SRVC_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_POSITION", "POSN_NAME", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_POSITION", "POSN_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_POSITION", "POSN_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_EMPLOYEE", "POSN_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_COMPANY", "AGENCY_FREE", c => c.Int());
            DropPrimaryKey("dbo.MT_SUB_SERVICE");
            DropPrimaryKey("dbo.MT_SERVICE");
            DropPrimaryKey("dbo.MT_PROJECT_D");
            DropPrimaryKey("dbo.MT_POSITION");
            DropColumn("dbo.TR_COMPANY_PERSON", "PERSON_SOURCE_CODE");
            DropColumn("dbo.TR_COMPANY_PERSON", "CONTACT_TYPE_CODE");
            DropColumn("dbo.MT_SUB_SERVICE", "SUB_SERVICE_NAME");
            DropColumn("dbo.MT_SUB_SERVICE", "SERVICE_ID");
            DropColumn("dbo.MT_SUB_SERVICE", "SUB_SERVICE_ID");
            DropColumn("dbo.MT_SERVICE", "SERVICE_NAME");
            DropColumn("dbo.MT_SERVICE", "SERVICE_ID");
            DropColumn("dbo.MT_PROJECT_D", "ACTUAL_FINISH_DATE");
            DropColumn("dbo.MT_PROJECT_D", "ACTUAL_START_DATE");
            DropColumn("dbo.MT_PROJECT_D", "SUB_SERVICE_ID");
            DropColumn("dbo.MT_PROJECT_D", "SERVICE_ID");
            DropColumn("dbo.MT_POSITION", "POSITION_NAME");
            DropColumn("dbo.MT_POSITION", "POSITION_CODE");
            DropColumn("dbo.MT_POSITION", "POSITION_ID");
            DropColumn("dbo.MT_EMPLOYEE", "POSITION_CODE");
            DropColumn("dbo.MT_COMPANY", "AGENCY_FEE");
            AddPrimaryKey("dbo.MT_SUB_SERVICE", new[] { "SUB_SRVC_ID", "SRVC_ID" });
            AddPrimaryKey("dbo.MT_SERVICE", "SRVC_ID");
            AddPrimaryKey("dbo.MT_PROJECT_D", new[] { "PROJECT_D_ID", "PROJECT_H_ID", "SRVC_ID", "SUB_SRVC_ID", "PROD_TYPE_CODE" });
            AddPrimaryKey("dbo.MT_POSITION", new[] { "POSN_ID", "POSN_CODE" });
        }
    }
}
