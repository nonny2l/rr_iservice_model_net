﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addJOB_NAMEontableMT_PROJECT_D : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PROJECT_D", "JOB_NAME", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PROJECT_D", "JOB_NAME");
        }
    }
}
