﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSEQfromMasterTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_JOB_STATUS", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.MT_POSITION", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.MT_SERVICE", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.MT_STATUS", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.MT_TASK_GROUP", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.MT_TASK_STATUS", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.TR_JOB_TASK", "ACCEPT_BY", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_JOB_TASK", "ACCEPT_DATE", c => c.DateTime());
            AddColumn("dbo.TR_JOB_TASK", "ACCEPT_STATUS", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_JOB_TASK", "ACCEPT_STATUS");
            DropColumn("dbo.TR_JOB_TASK", "ACCEPT_DATE");
            DropColumn("dbo.TR_JOB_TASK", "ACCEPT_BY");
            DropColumn("dbo.MT_TASK_STATUS", "SEQ");
            DropColumn("dbo.MT_TASK_GROUP", "SEQ");
            DropColumn("dbo.MT_STATUS", "SEQ");
            DropColumn("dbo.MT_SERVICE", "SEQ");
            DropColumn("dbo.MT_POSITION", "SEQ");
            DropColumn("dbo.MT_JOB_STATUS", "SEQ");
        }
    }
}
