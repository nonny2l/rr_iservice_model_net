﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetr_job : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB", "JOB_NAME", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_JOB", "JOB_NAME");
        }
    }
}
