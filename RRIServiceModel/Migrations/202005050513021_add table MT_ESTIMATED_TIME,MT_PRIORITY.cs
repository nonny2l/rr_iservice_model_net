﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableMT_ESTIMATED_TIMEMT_PRIORITY : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_ESTIMATED_TIME",
                c => new
                    {
                        ESTIMATED_ID = c.Int(nullable: false, identity: true),
                        ESTIMATED_CODE = c.String(maxLength: 100),
                        ESTIMATED_NAME = c.String(maxLength: 200),
                        REMARK = c.String(maxLength: 4000),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ESTIMATED_ID);
            
            CreateTable(
                "dbo.MT_PRIORITY",
                c => new
                    {
                        PRIORITY_ID = c.Int(nullable: false, identity: true),
                        PRIORITY_CODE = c.String(maxLength: 100),
                        PRIORITY_NAME = c.String(maxLength: 200),
                        REMARK = c.String(maxLength: 4000),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.PRIORITY_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_PRIORITY");
            DropTable("dbo.MT_ESTIMATED_TIME");
        }
    }
}
