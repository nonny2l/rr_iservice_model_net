﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcollumnESTIMATED_CODEinTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB_TASK", "ESTIMATED_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_TASK", "ESTIMATED_CODE", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_TASK", "ESTIMATED_CODE");
            DropColumn("dbo.TR_JOB_TASK", "ESTIMATED_CODE");
        }
    }
}
