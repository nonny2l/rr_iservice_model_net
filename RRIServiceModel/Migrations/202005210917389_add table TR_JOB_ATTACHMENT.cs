﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_JOB_ATTACHMENT : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_JOB_ATTACHMENT",
                c => new
                    {
                        JOB_ATTACHMENT_ID = c.Int(nullable: false, identity: true),
                        SEQ = c.Int(nullable: false),
                        JOB_NO = c.String(maxLength: 20),
                        FILE_NAME = c.String(maxLength: 4000),
                        OLD_FILE_NAME = c.String(maxLength: 4000),
                        PATH_FILE = c.String(maxLength: 4000),
                        FILE_TYPE = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.JOB_ATTACHMENT_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_JOB_ATTACHMENT");
        }
    }
}
