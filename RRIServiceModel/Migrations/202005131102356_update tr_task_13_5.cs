﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetr_task_13_5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_TASK", "TASK_STATUS_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_TASK", "COMP_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_TASK", "PLAN_START_DATE", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TR_TASK", "PLAN_END_DATE", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TR_TASK", "DUE_DATE", c => c.DateTime(nullable: false));
            DropColumn("dbo.TR_TASK", "PROGRESS_STATUS_CODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_TASK", "PROGRESS_STATUS_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_TASK", "DUE_DATE", c => c.DateTime());
            AlterColumn("dbo.TR_TASK", "PLAN_END_DATE", c => c.DateTime());
            AlterColumn("dbo.TR_TASK", "PLAN_START_DATE", c => c.DateTime());
            DropColumn("dbo.TR_TASK", "COMP_CODE");
            DropColumn("dbo.TR_TASK", "TASK_STATUS_CODE");
        }
    }
}
