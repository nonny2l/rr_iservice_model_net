﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableMT_JOB_STATUS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_JOB_STATUS",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        JOB_STATUS_CODE = c.String(maxLength: 100),
                        JOB_STATUS_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.MT_STATUS", "STATUS_CODE", c => c.String(maxLength: 100));
            DropColumn("dbo.MT_STATUS", "PROJECT_CODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MT_STATUS", "PROJECT_CODE", c => c.String(maxLength: 100));
            DropColumn("dbo.MT_STATUS", "STATUS_CODE");
            DropTable("dbo.MT_JOB_STATUS");
        }
    }
}
