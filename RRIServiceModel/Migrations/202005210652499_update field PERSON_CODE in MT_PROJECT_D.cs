﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatefieldPERSON_CODEinMT_PROJECT_D : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PROJECT_D", "PERSON_CODE", c => c.String(maxLength: 10));
            AddColumn("dbo.TR_JOB_TASK", "JOB_TASK_NAME", c => c.String(maxLength: 255));
            AddColumn("dbo.TR_JOB_TASK", "JOB_TASK_CODE", c => c.String(nullable: false, maxLength: 20));
            DropColumn("dbo.MT_PROJECT_D", "CONTACT_PERSON");
            DropColumn("dbo.TR_JOB_TASK", "TASK_JOB_NAME");
            DropColumn("dbo.TR_JOB_TASK", "TASK_JOB_CODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_JOB_TASK", "TASK_JOB_CODE", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.TR_JOB_TASK", "TASK_JOB_NAME", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_PROJECT_D", "CONTACT_PERSON", c => c.String(maxLength: 255));
            DropColumn("dbo.TR_JOB_TASK", "JOB_TASK_CODE");
            DropColumn("dbo.TR_JOB_TASK", "JOB_TASK_NAME");
            DropColumn("dbo.MT_PROJECT_D", "PERSON_CODE");
        }
    }
}
