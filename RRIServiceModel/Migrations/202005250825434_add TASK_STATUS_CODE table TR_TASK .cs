﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTASK_STATUS_CODEtableTR_TASK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_TASK", "TASK_STATUS_CODE", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_TASK", "TASK_STATUS_CODE");
        }
    }
}
