﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatebytom20200523_1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MT_NOTIFY_CONFIG");
            DropPrimaryKey("dbo.TR_TIME_SHEET");
            CreateTable(
                "dbo.MT_EMPLOYEE_POSITION",
                c => new
                    {
                        EMP_POSITION_ID = c.Int(nullable: false, identity: true),
                        EMP_NO = c.String(nullable: false, maxLength: 10),
                        SEQ = c.Int(nullable: false),
                        DEPT_CODE = c.String(nullable: false, maxLength: 3),
                        POSITION_CODE = c.String(nullable: false, maxLength: 3),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.EMP_POSITION_ID);
            
            CreateTable(
                "dbo.MT_PROJECT",
                c => new
                    {
                        PROJECT_ID = c.Int(nullable: false, identity: true),
                        PROJECT_CODE = c.String(nullable: false, maxLength: 3),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        PROJECT_NAME = c.String(nullable: false, maxLength: 100),
                        PROJECT_DESC = c.String(),
                        BUDGET_VALUE = c.Int(nullable: false),
                        BUDGET_UNIT_CODE = c.String(maxLength: 20),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PROJECT_ID, t.PROJECT_CODE, t.COMP_CODE });
            
            CreateTable(
                "dbo.TR_JOB",
                c => new
                    {
                        JOB_ID = c.Int(nullable: false, identity: true),
                        PROJECT_CODE = c.String(nullable: false, maxLength: 3),
                        JOB_STATUS_CODE = c.String(nullable: false, maxLength: 100),
                        SERVICE_CODE = c.String(nullable: false, maxLength: 3),
                        SUB_SERVICE_CODE = c.String(nullable: false, maxLength: 3),
                        JOB_NO = c.String(nullable: false, maxLength: 20),
                        JOB_DESC = c.String(),
                        WORK_BUDGET = c.Int(nullable: false),
                        DUE_DATE = c.DateTime(nullable: false),
                        BRIEF_DATE = c.DateTime(nullable: false),
                        ISSUE_BY = c.String(maxLength: 255),
                        ISSUE_DATE = c.DateTime(),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        PERSON_CODE = c.String(maxLength: 10),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.JOB_ID, t.PROJECT_CODE, t.JOB_STATUS_CODE, t.SERVICE_CODE, t.SUB_SERVICE_CODE });
            
            CreateTable(
                "dbo.TR_NOTIFY_READ",
                c => new
                    {
                        TR_NOTIFY__READ_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_MSG_CODE = c.String(nullable: false, maxLength: 100),
                        EMP_NO = c.String(nullable: false, maxLength: 10),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TR_NOTIFY__READ_ID);
            
            CreateTable(
                "dbo.TR_TASK_FOLLOW",
                c => new
                    {
                        TASK_FOLLOW_ID = c.Int(nullable: false, identity: true),
                        EMP_NO = c.String(maxLength: 10),
                        TASK_CODE = c.String(maxLength: 20),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TASK_FOLLOW_ID);
            
            CreateTable(
                "dbo.TR_TEAM_ASSIGN",
                c => new
                    {
                        TEAM_ASSIGN_ID = c.Int(nullable: false, identity: true),
                        TEAM_CODE = c.String(maxLength: 100),
                        EMP_NO = c.String(maxLength: 10),
                        IS_MAIN = c.String(maxLength: 1),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TEAM_ASSIGN_ID);
            
            AddColumn("dbo.MT_DEPARTMENT", "SEQ", c => c.Int(nullable: false));
            AddColumn("dbo.MT_EMPLOYEE", "PRE_NAME_CODE", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_TEAM_ROLE", "EMP_NO", c => c.String(maxLength: 10));
            AddColumn("dbo.MT_TEAM_ROLE", "PARENT_EMP_NO", c => c.String(maxLength: 10));
            AddColumn("dbo.MT_TEAM_ROLE", "DEPT_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_COMPANY_PERSON", "MOBILE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_JOB_ATTACHMENT", "FILE_PATH", c => c.String());
            AddColumn("dbo.TR_JOB_TASK", "JOB_TASK_STATUS_CODE", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_MSG", "REF_TABLE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_NOTIFY_MSG", "REF_KEY", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_NOTIFY_MSG", "REF_VALUE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_TIME_SHEET", "TIME_SHEET_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.TR_TIME_SHEET", "WORK_TIME", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_TOPIC_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_GROUP_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_HEADER_TEMPLATE", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_DETAIL_TEMPLATE", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.MT_NOTIFY_TOPIC", "NOTIFY_TOPIC_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_TOPIC", "NOTIFY_TOPIC_NAME", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.MT_TEAM_ROLE", "ROLE_NAME", c => c.String(maxLength: 100));
            AlterColumn("dbo.TR_JOB_ATTACHMENT", "FILE_NAME", c => c.String());
            AlterColumn("dbo.TR_JOB_ATTACHMENT", "OLD_FILE_NAME", c => c.String());
            AlterColumn("dbo.TR_JOB_ATTACHMENT", "FILE_TYPE", c => c.String());
            AlterColumn("dbo.TR_JOB_TASK", "JOB_TASK_NAME", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.TR_JOB_TASK", "JOB_NO", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.TR_JOB_TASK", "PROJECT_CODE", c => c.String(nullable: false, maxLength: 3));
            AlterColumn("dbo.TR_JOB_TASK", "DEPT_CODE", c => c.String(nullable: false, maxLength: 3));
            AlterColumn("dbo.TR_JOB_TASK", "COMP_CODE", c => c.String(nullable: false, maxLength: 3));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_CODE", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TOPIC_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_GROUP_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_HEADER", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_DESC", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.TR_NOTIFY_MSG", "EMP_NO", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.TR_TASK", "TASK_NAME", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.TR_TASK", "JOB_NO", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.TR_TASK", "PROJECT_CODE", c => c.String(nullable: false, maxLength: 3));
            AlterColumn("dbo.TR_TASK", "TASK_GROUP_CODE", c => c.String(nullable: false, maxLength: 3));
            AlterColumn("dbo.TR_TASK", "PLAN_START_DATE", c => c.DateTime());
            AlterColumn("dbo.TR_TASK", "PLAN_END_DATE", c => c.DateTime());
            AlterColumn("dbo.TR_TASK", "DUE_DATE", c => c.DateTime());
            AlterColumn("dbo.TR_TASK", "COMP_CODE", c => c.String(nullable: false, maxLength: 3));
            AddPrimaryKey("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_ID");
            AddPrimaryKey("dbo.TR_TIME_SHEET", "TIME_SHEET_ID");
            DropColumn("dbo.MT_DEPARTMENT", "JOB_HNDL_FLAG");
            DropColumn("dbo.MT_EMPLOYEE", "TITLE");
            DropColumn("dbo.MT_EMPLOYEE", "DEPT_CODE");
            DropColumn("dbo.MT_EMPLOYEE", "POSITION_CODE");
            DropColumn("dbo.MT_NOTIFY_CONFIG", "ID");
            DropColumn("dbo.MT_ORG_POSITION", "POSITION_NAME");
            DropColumn("dbo.MT_PROVINCE", "ZONE_PEA");
            DropColumn("dbo.MT_SUB_SERVICE", "AB_FLAG");
            DropColumn("dbo.MT_TEAM_ROLE", "USER_ID");
            DropColumn("dbo.MT_TEAM_ROLE", "PARENT_ID");
            DropColumn("dbo.TR_COMPANY_PERSON", "DEPT_CODE");
            DropColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION_CODE");
            DropColumn("dbo.TR_JOB_ATTACHMENT", "PATH_FILE");
            DropColumn("dbo.TR_JOB_TASK", "JOB_STATUS_CODE");
            DropColumn("dbo.TR_JOB_TASK", "ESTIMATED");
            DropColumn("dbo.TR_NOTIFY_MSG", "START_DATE");
            DropColumn("dbo.TR_NOTIFY_MSG", "END_DATE");
            DropColumn("dbo.TR_TASK", "PARENT_TASK_CODE");
            DropColumn("dbo.TR_TASK", "ACTUAL_START_DATE");
            DropColumn("dbo.TR_TASK", "ACTUAL_END_DATE");
            DropColumn("dbo.TR_TASK", "HASHTAG");
            DropColumn("dbo.TR_TASK", "TASK_STATUS_CODE");
            DropColumn("dbo.TR_TIME_SHEET", "TIMESHEET_ID");
            DropColumn("dbo.TR_TIME_SHEET", "WORKING_TIME");
            DropTable("dbo.MT_NOTIFY_GROUP");
            DropTable("dbo.MT_PROJECT_D");
            DropTable("dbo.MT_PROJECT_H");
            DropTable("dbo.MT_TASK_STATUS");
            DropTable("dbo.TR_NOTIFY_GROUP_ROLE");
            DropTable("dbo.TR_NOTIFY_USER_READ");
            DropTable("dbo.TR_NOTIFY_USER_ROLE");
            DropTable("dbo.TR_TASK_DOCUMENT");
            DropTable("dbo.TR_TASK_DOCUMENT_COMMENT");
            DropTable("dbo.TR_TEAM_ROLE");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TR_TEAM_ROLE",
                c => new
                    {
                        TEAM_ROLE_ID = c.Int(nullable: false, identity: true),
                        TEAM_CODE = c.String(maxLength: 100),
                        EMP_NO = c.String(maxLength: 3),
                        IS_MAIN = c.String(maxLength: 1),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TEAM_ROLE_ID);
            
            CreateTable(
                "dbo.TR_TASK_DOCUMENT_COMMENT",
                c => new
                    {
                        COMMENT_ID = c.Int(nullable: false, identity: true),
                        COMMENT_DESC = c.String(maxLength: 200),
                        TASK_CODE = c.String(nullable: false, maxLength: 20),
                        USER_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.COMMENT_ID);
            
            CreateTable(
                "dbo.TR_TASK_DOCUMENT",
                c => new
                    {
                        DOCUMENT_ID = c.Int(nullable: false, identity: true),
                        DOCUMENT_NAME = c.String(maxLength: 100),
                        DOCUMENT_URL = c.String(maxLength: 200),
                        TASK_CODE = c.String(nullable: false, maxLength: 11),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.DOCUMENT_ID);
            
            CreateTable(
                "dbo.TR_NOTIFY_USER_ROLE",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_MSG_CODE = c.String(maxLength: 20),
                        EMP_NO = c.String(maxLength: 10),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TR_NOTIFY_USER_READ",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_MSG_CODE = c.String(maxLength: 20),
                        EMP_NO = c.String(maxLength: 10),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TR_NOTIFY_GROUP_ROLE",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_MSG_CODE = c.String(maxLength: 20),
                        NOTIFY_GROUP_CODE = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_TASK_STATUS",
                c => new
                    {
                        STATUS_ID = c.Int(nullable: false, identity: true),
                        STATUS_CODE = c.String(maxLength: 20),
                        STATUS_NAME = c.String(maxLength: 100),
                        SEQ = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.STATUS_ID);
            
            CreateTable(
                "dbo.MT_PROJECT_H",
                c => new
                    {
                        PROJECT_H_ID = c.Int(nullable: false, identity: true),
                        PROJECT_CODE = c.String(nullable: false, maxLength: 3),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        PROJECT_NAME = c.String(nullable: false, maxLength: 100),
                        PROJECT_DESC = c.String(),
                        BUDGET_VALUE = c.Int(nullable: false),
                        BUDGET_UNIT_CODE = c.String(maxLength: 20),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PROJECT_H_ID, t.PROJECT_CODE, t.COMP_CODE });
            
            CreateTable(
                "dbo.MT_PROJECT_D",
                c => new
                    {
                        PROJECT_D_ID = c.Int(nullable: false, identity: true),
                        PROJECT_CODE = c.String(nullable: false, maxLength: 3),
                        SERVICE_CODE = c.String(nullable: false, maxLength: 3),
                        SUB_SERVICE_CODE = c.String(nullable: false, maxLength: 3),
                        PROD_TYPE_CODE = c.String(nullable: false, maxLength: 3),
                        JOB_NO = c.String(nullable: false, maxLength: 255),
                        JOB_DESC = c.String(),
                        JOB_DATE = c.DateTime(nullable: false),
                        JOB_STATUS_CODE = c.String(nullable: false),
                        STATUS_DATE = c.DateTime(nullable: false),
                        PERSON_CODE = c.String(maxLength: 10),
                        CANCEL_REASON = c.String(),
                        PE_REMK = c.String(),
                        PE_VAT_PCT = c.Int(),
                        PE_VAT_FLAG = c.String(maxLength: 1),
                        WORK_BUDGET = c.Int(nullable: false),
                        START_DATE = c.DateTime(nullable: false),
                        FINISH_DATE = c.DateTime(),
                        ACTUAL_START_DATE = c.DateTime(),
                        ACTUAL_FINISH_DATE = c.DateTime(),
                        ISSUE_BY = c.String(maxLength: 255),
                        ISSUE_DATE = c.DateTime(),
                        ACK_BY = c.String(maxLength: 255),
                        ACK_DATE = c.DateTime(),
                        APPV_BY = c.String(maxLength: 255),
                        APPV_DATE = c.DateTime(),
                        ASSIGN_TO = c.Int(),
                        ASSIGN_DATE = c.DateTime(),
                        FREEZE_FLAG = c.String(maxLength: 1),
                        FREEZE_YYMM = c.String(maxLength: 4),
                        CLIENT_APPV_BY = c.String(maxLength: 255),
                        CLIENT_APPV_DATE = c.DateTime(),
                        PE_AGCY_FEE_PCT = c.Int(),
                        LAST_VER_NO = c.Int(),
                        LAST_VER_DATE = c.DateTime(),
                        VER_LOCK = c.String(maxLength: 1),
                        PE_AGCY_FEE_AMT = c.Int(nullable: false),
                        REMARK = c.String(),
                        JOB_NAME = c.String(maxLength: 255),
                        BRIEF_DATE = c.DateTime(nullable: false),
                        COMP_CODE = c.String(maxLength: 3),
                        DUE_DATE = c.DateTime(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PROJECT_D_ID, t.PROJECT_CODE, t.SERVICE_CODE, t.SUB_SERVICE_CODE, t.PROD_TYPE_CODE });
            
            CreateTable(
                "dbo.MT_NOTIFY_GROUP",
                c => new
                    {
                        NOTIFY_GROUP_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_GROUP_CODE = c.String(maxLength: 20),
                        NOTIFY_GROUP_VALUE = c.String(maxLength: 100),
                        NOTIFY_GROUP_NAME = c.String(maxLength: 100),
                        NOTIFY_GROUP_DESC = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTIFY_GROUP_ID);
            
            AddColumn("dbo.TR_TIME_SHEET", "WORKING_TIME", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_TIME_SHEET", "TIMESHEET_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.TR_TASK", "TASK_STATUS_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_TASK", "HASHTAG", c => c.String());
            AddColumn("dbo.TR_TASK", "ACTUAL_END_DATE", c => c.DateTime());
            AddColumn("dbo.TR_TASK", "ACTUAL_START_DATE", c => c.DateTime());
            AddColumn("dbo.TR_TASK", "PARENT_TASK_CODE", c => c.String(maxLength: 11));
            AddColumn("dbo.TR_NOTIFY_MSG", "END_DATE", c => c.DateTime());
            AddColumn("dbo.TR_NOTIFY_MSG", "START_DATE", c => c.DateTime());
            AddColumn("dbo.TR_JOB_TASK", "ESTIMATED", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_JOB_TASK", "JOB_STATUS_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_JOB_ATTACHMENT", "PATH_FILE", c => c.String(maxLength: 4000));
            AddColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_COMPANY_PERSON", "DEPT_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.MT_TEAM_ROLE", "PARENT_ID", c => c.Int());
            AddColumn("dbo.MT_TEAM_ROLE", "USER_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_SUB_SERVICE", "AB_FLAG", c => c.String(nullable: false, maxLength: 1));
            AddColumn("dbo.MT_PROVINCE", "ZONE_PEA", c => c.String(maxLength: 10));
            AddColumn("dbo.MT_ORG_POSITION", "POSITION_NAME", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_NOTIFY_CONFIG", "ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_EMPLOYEE", "POSITION_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_EMPLOYEE", "DEPT_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_EMPLOYEE", "TITLE", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.MT_DEPARTMENT", "JOB_HNDL_FLAG", c => c.String(maxLength: 255));
            DropPrimaryKey("dbo.TR_TIME_SHEET");
            DropPrimaryKey("dbo.MT_NOTIFY_CONFIG");
            AlterColumn("dbo.TR_TASK", "COMP_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_TASK", "DUE_DATE", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TR_TASK", "PLAN_END_DATE", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TR_TASK", "PLAN_START_DATE", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TR_TASK", "TASK_GROUP_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_TASK", "PROJECT_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_TASK", "JOB_NO", c => c.String(maxLength: 20));
            AlterColumn("dbo.TR_TASK", "TASK_NAME", c => c.String(maxLength: 300));
            AlterColumn("dbo.TR_NOTIFY_MSG", "EMP_NO", c => c.String(maxLength: 10));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_DESC", c => c.String(maxLength: 255));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_HEADER", c => c.String(maxLength: 255));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_GROUP_CODE", c => c.String(maxLength: 20));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TOPIC_CODE", c => c.String(maxLength: 20));
            AlterColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_CODE", c => c.String(maxLength: 100));
            AlterColumn("dbo.TR_JOB_TASK", "COMP_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_JOB_TASK", "DEPT_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_JOB_TASK", "PROJECT_CODE", c => c.String(maxLength: 3));
            AlterColumn("dbo.TR_JOB_TASK", "JOB_NO", c => c.String(maxLength: 100));
            AlterColumn("dbo.TR_JOB_TASK", "JOB_TASK_NAME", c => c.String(maxLength: 255));
            AlterColumn("dbo.TR_JOB_ATTACHMENT", "FILE_TYPE", c => c.String(maxLength: 100));
            AlterColumn("dbo.TR_JOB_ATTACHMENT", "OLD_FILE_NAME", c => c.String(maxLength: 4000));
            AlterColumn("dbo.TR_JOB_ATTACHMENT", "FILE_NAME", c => c.String(maxLength: 4000));
            AlterColumn("dbo.MT_TEAM_ROLE", "ROLE_NAME", c => c.String(maxLength: 255));
            AlterColumn("dbo.MT_NOTIFY_TOPIC", "NOTIFY_TOPIC_NAME", c => c.String(maxLength: 100));
            AlterColumn("dbo.MT_NOTIFY_TOPIC", "NOTIFY_TOPIC_CODE", c => c.String(maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_DETAIL_TEMPLATE", c => c.String(maxLength: 255));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_HEADER_TEMPLATE", c => c.String(maxLength: 100));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_GROUP_CODE", c => c.String(maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_TOPIC_CODE", c => c.String(maxLength: 20));
            AlterColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_CODE", c => c.String(maxLength: 20));
            DropColumn("dbo.TR_TIME_SHEET", "WORK_TIME");
            DropColumn("dbo.TR_TIME_SHEET", "TIME_SHEET_ID");
            DropColumn("dbo.TR_NOTIFY_MSG", "REF_VALUE");
            DropColumn("dbo.TR_NOTIFY_MSG", "REF_KEY");
            DropColumn("dbo.TR_NOTIFY_MSG", "REF_TABLE");
            DropColumn("dbo.TR_JOB_TASK", "JOB_TASK_STATUS_CODE");
            DropColumn("dbo.TR_JOB_ATTACHMENT", "FILE_PATH");
            DropColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION");
            DropColumn("dbo.TR_COMPANY_PERSON", "MOBILE");
            DropColumn("dbo.MT_TEAM_ROLE", "DEPT_CODE");
            DropColumn("dbo.MT_TEAM_ROLE", "PARENT_EMP_NO");
            DropColumn("dbo.MT_TEAM_ROLE", "EMP_NO");
            DropColumn("dbo.MT_NOTIFY_CONFIG", "NOTIFY_CONFIG_ID");
            DropColumn("dbo.MT_EMPLOYEE", "PRE_NAME_CODE");
            DropColumn("dbo.MT_DEPARTMENT", "SEQ");
            DropTable("dbo.TR_TEAM_ASSIGN");
            DropTable("dbo.TR_TASK_FOLLOW");
            DropTable("dbo.TR_NOTIFY_READ");
            DropTable("dbo.TR_JOB");
            DropTable("dbo.MT_PROJECT");
            DropTable("dbo.MT_EMPLOYEE_POSITION");
            AddPrimaryKey("dbo.TR_TIME_SHEET", "TIMESHEET_ID");
            AddPrimaryKey("dbo.MT_NOTIFY_CONFIG", "ID");
        }
    }
}
