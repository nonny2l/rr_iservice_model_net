﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_ASSIGN_TO : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_ASSIGN_TO",
                c => new
                    {
                        ASSIGN_TO_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        EMAIL = c.String(),
                        TASK_CODE = c.String(nullable: false, maxLength: 128),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ASSIGN_TO_ID, t.USER_ID, t.TASK_CODE });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_ASSIGN_TO");
        }
    }
}
