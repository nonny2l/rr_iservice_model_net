﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addjob_year : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB", "JOB_YEAR", c => c.String(nullable: false, maxLength: 4));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_JOB", "JOB_YEAR");
        }
    }
}
