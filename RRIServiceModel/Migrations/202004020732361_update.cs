﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_CONFIG_D", "CONFIG_D_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.MT_PROVINCE", "ZONE_PEA", c => c.String(maxLength: 10));
            AddColumn("dbo.TR_COMPANY_ADDRESS", "DISTRICT_CODE", c => c.Int(nullable: false));
            AddColumn("dbo.TR_COMPANY_ADDRESS", "SUB_DISTRICT_CODE", c => c.Int(nullable: false));
            AddColumn("dbo.TR_COMPANY_ADDRESS", "PROV_CODE", c => c.Int(nullable: false));
            AlterColumn("dbo.TR_COMPANY_REGISTER", "AREA_ROOF_SQM", c => c.String(maxLength: 100));
            DropColumn("dbo.TR_COMPANY_ADDRESS", "DISTRICT_NAME");
            DropColumn("dbo.TR_COMPANY_ADDRESS", "SUB_DISTRICT_NAME");
            DropColumn("dbo.TR_COMPANY_ADDRESS", "PROV_NAME");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_COMPANY_ADDRESS", "PROV_NAME", c => c.String(maxLength: 2));
            AddColumn("dbo.TR_COMPANY_ADDRESS", "SUB_DISTRICT_NAME", c => c.String(maxLength: 6));
            AddColumn("dbo.TR_COMPANY_ADDRESS", "DISTRICT_NAME", c => c.String(maxLength: 4));
            AlterColumn("dbo.TR_COMPANY_REGISTER", "AREA_ROOF_SQM", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.TR_COMPANY_ADDRESS", "PROV_CODE");
            DropColumn("dbo.TR_COMPANY_ADDRESS", "SUB_DISTRICT_CODE");
            DropColumn("dbo.TR_COMPANY_ADDRESS", "DISTRICT_CODE");
            DropColumn("dbo.MT_PROVINCE", "ZONE_PEA");
            DropColumn("dbo.MT_CONFIG_D", "CONFIG_D_CODE");
        }
    }
}
