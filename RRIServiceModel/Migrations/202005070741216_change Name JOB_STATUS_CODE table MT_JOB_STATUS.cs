﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeNameJOB_STATUS_CODEtableMT_JOB_STATUS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PROJECT_D", "JOB_STATUS_CODE", c => c.String(nullable: false));
            DropColumn("dbo.MT_PROJECT_D", "JOB_STATUS");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MT_PROJECT_D", "JOB_STATUS", c => c.Int(nullable: false));
            DropColumn("dbo.MT_PROJECT_D", "JOB_STATUS_CODE");
        }
    }
}
