﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_DOC_RUNNING : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_DOC_RUNNING",
                c => new
                    {
                        DOCUMENT_RUNNING_ID = c.Int(nullable: false, identity: true),
                        COM_CODE = c.String(),
                        DEPT_CODE = c.String(),
                        PREFIX_CODE = c.String(),
                        PREFIX_YEAR = c.Int(nullable: false),
                        PREFIX_MONTH = c.Int(nullable: false),
                        DOCUMENT_RUNNING = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.DOCUMENT_RUNNING_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_DOC_RUNNING");
        }
    }
}
