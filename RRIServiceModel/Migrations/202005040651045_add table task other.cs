﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtabletaskother : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_TASK_GROUP",
                c => new
                    {
                        GROUP_ID = c.Int(nullable: false, identity: true),
                        GROUP_NAME = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.GROUP_ID);
            
            CreateTable(
                "dbo.MT_TASK_STATUS",
                c => new
                    {
                        STATUS_ID = c.Int(nullable: false, identity: true),
                        STATUS_NAME = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.STATUS_ID);
            
            CreateTable(
                "dbo.TR_TASK_DOCUMENT",
                c => new
                    {
                        DOCUMENT_ID = c.Int(nullable: false, identity: true),
                        DOCUMENT_NAME = c.String(maxLength: 100),
                        DOCUMENT_URL = c.String(maxLength: 200),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.DOCUMENT_ID);
            
            CreateTable(
                "dbo.TR_TASK_DOCUMENT_COMMENT",
                c => new
                    {
                        COMMENT_ID = c.Int(nullable: false, identity: true),
                        COMMENT_DESC = c.String(maxLength: 200),
                        USER_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.COMMENT_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_TASK_DOCUMENT_COMMENT");
            DropTable("dbo.TR_TASK_DOCUMENT");
            DropTable("dbo.MT_TASK_STATUS");
            DropTable("dbo.MT_TASK_GROUP");
        }
    }
}
