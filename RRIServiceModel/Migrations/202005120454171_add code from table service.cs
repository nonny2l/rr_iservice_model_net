﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcodefromtableservice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_SERVICE", "SERVICE_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_SUB_SERVICE", "SUB_SERVICE_CODE", c => c.String(nullable: false, maxLength: 3));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_SUB_SERVICE", "SUB_SERVICE_CODE");
            DropColumn("dbo.MT_SERVICE", "SERVICE_CODE");
        }
    }
}
