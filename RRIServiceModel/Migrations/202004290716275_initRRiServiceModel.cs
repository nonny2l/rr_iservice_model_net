﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initRRiServiceModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LOG_API",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        REQUEST = c.String(),
                        RESPONSE = c.String(),
                        STATUS = c.String(maxLength: 50),
                        REQUEST_DATETIME = c.DateTime(),
                        RESPONSE_DATETIME = c.DateTime(),
                        MODULE_TYPE = c.String(maxLength: 100),
                        ACTION_TYPE = c.String(maxLength: 100),
                        USER_ACCOUNT = c.String(maxLength: 100),
                        IP = c.String(maxLength: 50),
                        CREATE_BY = c.String(maxLength: 30),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 30),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30),
                        DELETE_DATE = c.DateTime(),
                        TRANSACTION_ID = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_COMPANY",
                c => new
                    {
                        COMP_ID = c.Int(nullable: false),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        COMP_NAME = c.String(nullable: false, maxLength: 100),
                        BUSINESS_TYPE = c.String(maxLength: 200),
                        SUB_BUSINESS_TYPE = c.String(maxLength: 200),
                        START_DATE = c.DateTime(),
                        END_DATE = c.DateTime(),
                        TAX_ID = c.String(maxLength: 20),
                        TEL_NO = c.String(maxLength: 100),
                        FAX_NO = c.String(maxLength: 100),
                        TERM_OF_PAYMENT = c.Int(),
                        AGENCY_FREE = c.Int(),
                        FACEBOOK_ID = c.String(maxLength: 150),
                        WEBSITE = c.String(maxLength: 255),
                        IS_COMPANY = c.String(nullable: false, maxLength: 1),
                        REMARK = c.String(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.COMP_ID, t.COMP_CODE });
            
            CreateTable(
                "dbo.MT_CONFIG_D",
                c => new
                    {
                        CONFIG_D_ID = c.Int(nullable: false, identity: true),
                        CONFIG_H_ID = c.Int(nullable: false),
                        CONFIG_D_CODE = c.String(maxLength: 60),
                        CONFIG_D_SEQ = c.Int(nullable: false),
                        CONFIG_D_TNAME = c.String(),
                        CONFIG_D_ENAME = c.String(),
                        PARENT_D_ID = c.Int(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.CONFIG_D_ID, t.CONFIG_H_ID });
            
            CreateTable(
                "dbo.MT_CONFIG_H",
                c => new
                    {
                        CONFIG_H_ID = c.Int(nullable: false, identity: true),
                        CONFIG_H_CODE = c.String(maxLength: 20),
                        CONFIG_H_TNAME = c.String(maxLength: 100),
                        CONFIG_H_ENAME = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.CONFIG_H_ID);
            
            CreateTable(
                "dbo.MT_DEPARTMENT",
                c => new
                    {
                        DEPT_ID = c.Int(nullable: false, identity: true),
                        DEPT_CODE = c.String(nullable: false, maxLength: 3),
                        DEPT_NAME = c.String(maxLength: 255),
                        JOB_HNDL_FLAG = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.DEPT_ID, t.DEPT_CODE });
            
            CreateTable(
                "dbo.MT_DISTRICT",
                c => new
                    {
                        DISTRICT_CODE = c.String(nullable: false, maxLength: 4),
                        DISTRICT_TNAME = c.String(nullable: false, maxLength: 200),
                        DISTRICT_ENAME = c.String(maxLength: 200),
                        REF_PROV_CODE = c.String(nullable: false, maxLength: 2),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.DISTRICT_CODE);
            
            CreateTable(
                "dbo.MT_EMPLOYEE",
                c => new
                    {
                        EMP_ID = c.Int(nullable: false, identity: true),
                        EMP_NO = c.String(nullable: false, maxLength: 10),
                        TITLE = c.String(nullable: false, maxLength: 100),
                        FIRST_NAME = c.String(nullable: false, maxLength: 255),
                        LAST_NAME = c.String(nullable: false, maxLength: 255),
                        START_DATE = c.DateTime(),
                        END_DATE = c.DateTime(),
                        DEPT_CODE = c.String(nullable: false, maxLength: 3),
                        POSN_CODE = c.String(nullable: false, maxLength: 3),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.EMP_ID, t.EMP_NO });
            
            CreateTable(
                "dbo.MT_MENU",
                c => new
                    {
                        MENU_ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(maxLength: 100),
                        DESCRIPTION = c.String(maxLength: 255),
                        URL = c.String(maxLength: 255),
                        ICON = c.String(maxLength: 50),
                        SORT = c.Int(),
                        PARENT_ID = c.Int(),
                        MENU_TYPE_ID = c.Int(),
                        MENU_GROUP_ID = c.Int(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.MENU_ID);
            
            CreateTable(
                "dbo.MT_MENU_GROUP",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(maxLength: 50),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_MENU_GROUP_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MENU_ID = c.Int(nullable: false),
                        GROUP_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.MENU_ID, t.GROUP_ID });
            
            CreateTable(
                "dbo.MT_MENU_ROLE_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MENU_ID = c.Int(nullable: false),
                        ROLE_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.MENU_ID, t.ROLE_ID });
            
            CreateTable(
                "dbo.MT_MENU_TYPE",
                c => new
                    {
                        MENU_TYPE_ID = c.Int(nullable: false, identity: true),
                        MENU_TYPE_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.MENU_TYPE_ID);
            
            CreateTable(
                "dbo.MT_MENU_TYPE_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MENU_ID = c.Int(nullable: false),
                        TYPE_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.MENU_ID, t.TYPE_ID });
            
            CreateTable(
                "dbo.MT_PERSON",
                c => new
                    {
                        PERSON_ID = c.Int(nullable: false, identity: true),
                        TITLE_NAME = c.String(nullable: false, maxLength: 100),
                        FIRST_NAME = c.String(nullable: false, maxLength: 100),
                        LAST_NAME = c.String(nullable: false, maxLength: 100),
                        TELEPHONE = c.String(maxLength: 255),
                        EMAIL = c.String(maxLength: 20),
                        MOBILE = c.String(maxLength: 20),
                        CITIZEN_ID = c.String(maxLength: 13),
                        PASSPORT = c.String(maxLength: 20),
                        LINE_ID = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.PERSON_ID);
            
            CreateTable(
                "dbo.MT_POSITION",
                c => new
                    {
                        POSN_ID = c.Int(nullable: false, identity: true),
                        POSN_CODE = c.String(nullable: false, maxLength: 3),
                        POSN_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.POSN_ID, t.POSN_CODE });
            
            CreateTable(
                "dbo.MT_PRODUCT_TYPE",
                c => new
                    {
                        PROD_TYPE_ID = c.Int(nullable: false, identity: true),
                        PROD_TYPE_CODE = c.String(nullable: false, maxLength: 3),
                        PROD_TYPE_NAME = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PROD_TYPE_ID, t.PROD_TYPE_CODE });
            
            CreateTable(
                "dbo.MT_PROJECT_D",
                c => new
                    {
                        PROJECT_D_ID = c.Int(nullable: false, identity: true),
                        PROJECT_H_ID = c.Int(nullable: false),
                        SRVC_ID = c.Int(nullable: false),
                        SUB_SRVC_ID = c.Int(nullable: false),
                        PROD_TYPE_CODE = c.String(nullable: false, maxLength: 3),
                        JOB_NO = c.String(nullable: false, maxLength: 255),
                        JOB_DESC = c.String(),
                        JOB_DATE = c.DateTime(nullable: false),
                        JOB_STATUS = c.Int(nullable: false),
                        STATUS_DATE = c.DateTime(nullable: false),
                        CONTACT_PERSON = c.String(maxLength: 255),
                        CANCEL_REASON = c.String(),
                        PE_REMK = c.String(),
                        PE_VAT_PCT = c.Int(),
                        PE_VAT_FLAG = c.String(maxLength: 1),
                        WORK_BUDGET = c.Int(nullable: false),
                        START_DATE = c.DateTime(nullable: false),
                        FINISH_DATE = c.DateTime(),
                        ACTL_START_DATE = c.DateTime(),
                        ACTL_FINISH_DATE = c.DateTime(),
                        ISSUE_BY = c.String(maxLength: 255),
                        ISSUE_DATE = c.DateTime(),
                        ACK_BY = c.String(maxLength: 255),
                        ACK_DATE = c.DateTime(),
                        APPV_BY = c.String(maxLength: 255),
                        APPV_DATE = c.DateTime(),
                        ASSIGN_TO = c.Int(),
                        ASSIGN_DATE = c.DateTime(),
                        FREEZE_FLAG = c.String(maxLength: 1),
                        FREEZE_YYMM = c.String(maxLength: 4),
                        CLIENT_APPV_BY = c.String(maxLength: 255),
                        CLIENT_APPV_DATE = c.DateTime(),
                        PE_AGCY_FEE_PCT = c.Int(),
                        LAST_VER_NO = c.Int(),
                        LAST_VER_DATE = c.DateTime(),
                        VER_LOCK = c.String(maxLength: 1),
                        PE_AGCY_FEE_AMT = c.Int(nullable: false),
                        REMARK = c.String(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PROJECT_D_ID, t.PROJECT_H_ID, t.SRVC_ID, t.SUB_SRVC_ID, t.PROD_TYPE_CODE });
            
            CreateTable(
                "dbo.MT_PROJECT_H",
                c => new
                    {
                        PROJECT_H_ID = c.Int(nullable: false, identity: true),
                        PROJECT_CODE = c.String(nullable: false, maxLength: 3),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        PROJECT_NAME = c.String(nullable: false, maxLength: 100),
                        PROJECT_DESC = c.String(),
                        BUDGET_VALUE = c.Int(nullable: false),
                        BUDGET_UNIT_TYPE_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PROJECT_H_ID, t.PROJECT_CODE, t.COMP_CODE });
            
            CreateTable(
                "dbo.MT_PROVINCE",
                c => new
                    {
                        PROV_CODE = c.String(nullable: false, maxLength: 2),
                        PROV_TNAME = c.String(nullable: false, maxLength: 100),
                        PROV_ENAME = c.String(maxLength: 100),
                        PROV_REGION = c.String(nullable: false, maxLength: 50),
                        ZONE_PEA = c.String(maxLength: 10),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.PROV_CODE);
            
            CreateTable(
                "dbo.MT_SERVICE",
                c => new
                    {
                        SRVC_ID = c.Int(nullable: false, identity: true),
                        SRVC_NAME = c.String(nullable: false, maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.SRVC_ID);
            
            CreateTable(
                "dbo.MT_SUB_SERVICE",
                c => new
                    {
                        SUB_SRVC_ID = c.Int(nullable: false, identity: true),
                        SRVC_ID = c.Int(nullable: false),
                        SUB_SRVC_NAME = c.String(nullable: false, maxLength: 100),
                        AB_FLAG = c.String(nullable: false, maxLength: 1),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.SUB_SRVC_ID, t.SRVC_ID });
            
            CreateTable(
                "dbo.MT_SUBDISTRICT",
                c => new
                    {
                        SUB_DISTRICT_CODE = c.String(nullable: false, maxLength: 6),
                        SUB_DISTRICT_TNAME = c.String(nullable: false, maxLength: 200),
                        SUB_DISTRICT_ENAME = c.String(maxLength: 200),
                        REF_DISTRICT_CODE = c.String(nullable: false, maxLength: 4),
                        ZIPCODE = c.String(nullable: false, maxLength: 5),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.SUB_DISTRICT_CODE);
            
            CreateTable(
                "dbo.MT_TEAM_ROLE",
                c => new
                    {
                        TEAM_ROLE_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        PARENT_ID = c.Int(),
                        ROLE_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TEAM_ROLE_ID);
            
            CreateTable(
                "dbo.MT_USER",
                c => new
                    {
                        USER_ID = c.Int(nullable: false, identity: true),
                        USERNAME = c.String(maxLength: 60),
                        PASSWORD = c.String(maxLength: 60),
                        USER_GROUP_ID = c.Int(),
                        REMEMBER_TOKEN = c.String(maxLength: 100),
                        USER_TYPE_ID = c.Int(nullable: false),
                        EMP_NO = c.String(maxLength: 10),
                        AGENT_NO = c.String(maxLength: 10),
                        NAME = c.String(maxLength: 255),
                        AVATAR = c.String(maxLength: 255),
                        EMAIL = c.String(maxLength: 255),
                        REMARK = c.String(maxLength: 1000),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.USER_ID);
            
            CreateTable(
                "dbo.MT_USER_GROUP",
                c => new
                    {
                        USER_GROUP_ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(maxLength: 40),
                        DESCRIPTION = c.String(maxLength: 100),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.USER_GROUP_ID);
            
            CreateTable(
                "dbo.MT_USER_GROUP_ROLE",
                c => new
                    {
                        USER_GROUP_ROLE_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        USER_GROUP_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.USER_GROUP_ROLE_ID, t.USER_ID, t.USER_GROUP_ID });
            
            CreateTable(
                "dbo.MT_USER_MENU_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        MENU_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.USER_ID, t.MENU_ID });
            
            CreateTable(
                "dbo.MT_USER_ROLE",
                c => new
                    {
                        ROLE_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        ROLE_NAME = c.String(maxLength: 50),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ROLE_ID, t.USER_ID });
            
            CreateTable(
                "dbo.MT_USER_TYPE",
                c => new
                    {
                        USER_TYPE_ID = c.Int(nullable: false, identity: true),
                        USER_TYPE_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.USER_TYPE_ID);
            
            CreateTable(
                "dbo.TR_COMPANY_ADDRESS",
                c => new
                    {
                        COMP_ADDR_ID = c.Int(nullable: false, identity: true),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        ADDRESS = c.String(maxLength: 255),
                        DISTRICT_CODE = c.String(maxLength: 4),
                        SUB_DISTRICT_CODE = c.String(maxLength: 6),
                        PROV_CODE = c.String(maxLength: 2),
                        ZIPCODE = c.String(maxLength: 5),
                        ZONE = c.String(maxLength: 20),
                        IS_MAIN = c.String(nullable: false, maxLength: 1),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.COMP_ADDR_ID, t.COMP_CODE });
            
            CreateTable(
                "dbo.TR_COMPANY_PERSON",
                c => new
                    {
                        COMP_PERSON_ID = c.Int(nullable: false, identity: true),
                        PERSON_ID = c.Int(nullable: false),
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        DEPARTMENT = c.String(maxLength: 100),
                        JOB_POSITION = c.String(maxLength: 100),
                        EMAIL = c.String(maxLength: 100),
                        CONTACT_TYPE_CFCODE = c.String(maxLength: 60),
                        PERSON_SOURCE_CFCODE = c.String(maxLength: 60),
                        IS_MAIN = c.String(maxLength: 1),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.COMP_PERSON_ID, t.PERSON_ID, t.COMP_CODE });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_COMPANY_PERSON");
            DropTable("dbo.TR_COMPANY_ADDRESS");
            DropTable("dbo.MT_USER_TYPE");
            DropTable("dbo.MT_USER_ROLE");
            DropTable("dbo.MT_USER_MENU_PERMISSION");
            DropTable("dbo.MT_USER_GROUP_ROLE");
            DropTable("dbo.MT_USER_GROUP");
            DropTable("dbo.MT_USER");
            DropTable("dbo.MT_TEAM_ROLE");
            DropTable("dbo.MT_SUBDISTRICT");
            DropTable("dbo.MT_SUB_SERVICE");
            DropTable("dbo.MT_SERVICE");
            DropTable("dbo.MT_PROVINCE");
            DropTable("dbo.MT_PROJECT_H");
            DropTable("dbo.MT_PROJECT_D");
            DropTable("dbo.MT_PRODUCT_TYPE");
            DropTable("dbo.MT_POSITION");
            DropTable("dbo.MT_PERSON");
            DropTable("dbo.MT_MENU_TYPE_PERMISSION");
            DropTable("dbo.MT_MENU_TYPE");
            DropTable("dbo.MT_MENU_ROLE_PERMISSION");
            DropTable("dbo.MT_MENU_GROUP_PERMISSION");
            DropTable("dbo.MT_MENU_GROUP");
            DropTable("dbo.MT_MENU");
            DropTable("dbo.MT_EMPLOYEE");
            DropTable("dbo.MT_DISTRICT");
            DropTable("dbo.MT_DEPARTMENT");
            DropTable("dbo.MT_CONFIG_H");
            DropTable("dbo.MT_CONFIG_D");
            DropTable("dbo.MT_COMPANY");
            DropTable("dbo.LOG_API");
        }
    }
}
