﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeidtocodetask : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.TR_ASSIGN_TO");
            AddColumn("dbo.TR_ASSIGN_TO", "EMP_NO", c => c.String(nullable: false, maxLength: 10));
            AddColumn("dbo.TR_TASK", "PROJECT_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.TR_TASK", "TASK_GROUP_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.TR_TASK", "PRIORITY_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.TR_TASK", "PROGRESS_STATUS_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.TR_TIME_SHEET", "EMP_NO", c => c.String(maxLength: 10));
            AddPrimaryKey("dbo.TR_ASSIGN_TO", new[] { "ASSIGN_TO_ID", "EMP_NO", "TASK_CODE" });
            DropColumn("dbo.TR_ASSIGN_TO", "USER_ID");
            DropColumn("dbo.TR_TASK", "PROJECT_H_ID");
            DropColumn("dbo.TR_TASK", "TASK_GROUP_ID");
            DropColumn("dbo.TR_TASK", "PRIORITY_ID");
            DropColumn("dbo.TR_TASK", "PROGRESS_STATUS_ID");
            DropColumn("dbo.TR_TIME_SHEET", "USER_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_TIME_SHEET", "USER_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_TASK", "PROGRESS_STATUS_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_TASK", "PRIORITY_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_TASK", "TASK_GROUP_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_TASK", "PROJECT_H_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_ASSIGN_TO", "USER_ID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.TR_ASSIGN_TO");
            DropColumn("dbo.TR_TIME_SHEET", "EMP_NO");
            DropColumn("dbo.TR_TASK", "PROGRESS_STATUS_CODE");
            DropColumn("dbo.TR_TASK", "PRIORITY_CODE");
            DropColumn("dbo.TR_TASK", "TASK_GROUP_CODE");
            DropColumn("dbo.TR_TASK", "PROJECT_CODE");
            DropColumn("dbo.TR_ASSIGN_TO", "EMP_NO");
            AddPrimaryKey("dbo.TR_ASSIGN_TO", new[] { "ASSIGN_TO_ID", "USER_ID", "TASK_CODE" });
        }
    }
}
