﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeEMP_NOtableTR_NOTIFY_MSG : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_NOTIFY_MSG", "EMP_NO", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_NOTIFY_MSG", "EMP_NO");
        }
    }
}
