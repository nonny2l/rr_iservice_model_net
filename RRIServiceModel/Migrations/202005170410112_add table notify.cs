﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtablenotify : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NOTIFY_GROUP",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_ID = c.Int(),
                        USER_GROUP_ID = c.Int(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_NOTIFY_MSG",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_TYPE_ID = c.Int(),
                        NOTIFY_GROUP_ID = c.Int(),
                        NOTIFY_TOPIC = c.String(maxLength: 255),
                        NOTIFY_DESC = c.String(maxLength: 255),
                        IMG_URL = c.String(maxLength: 255),
                        URL = c.String(maxLength: 255),
                        START_DATE = c.DateTime(),
                        END_DATE = c.DateTime(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_NOTIFY_TYPE",
                c => new
                    {
                        NOTIFY_TYPE_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_TYPE_NAME = c.String(maxLength: 255),
                        NOTITY_TYPE_DESC = c.String(maxLength: 255),
                        ACTION_TYPE = c.String(maxLength: 20),
                        ACTION_CONDDITION_TYPE = c.String(maxLength: 50),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTIFY_TYPE_ID);
            
            CreateTable(
                "dbo.NOTIFY_USER",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_ID = c.Int(),
                        USER_ID = c.Int(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TR_NOTIFY_MSG",
                c => new
                    {
                        NOTIFY_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_TYPE_ID = c.Int(),
                        NOTIFY_GROUP_ID = c.Int(),
                        NOTIFY_TOPIC = c.String(maxLength: 255),
                        NOTIFY_DESC = c.String(maxLength: 255),
                        IMG_URL = c.String(maxLength: 255),
                        URL = c.String(maxLength: 255),
                        START_DATE = c.DateTime(),
                        END_DATE = c.DateTime(),
                        MSG_USER_CODE = c.String(),
                        MSG_USER_TYPE = c.String(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTIFY_ID);
            
            CreateTable(
                "dbo.TR_NOTIFY_USER_READ",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_ID = c.Int(),
                        USER_ID = c.Int(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.TR_NOTI");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TR_NOTI",
                c => new
                    {
                        NOTI_ID = c.Int(nullable: false, identity: true),
                        NOTI_NAME = c.String(),
                        NOTI_SUBJECT = c.String(),
                        NOTI_DESC = c.String(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTI_ID);
            
            DropTable("dbo.TR_NOTIFY_USER_READ");
            DropTable("dbo.TR_NOTIFY_MSG");
            DropTable("dbo.NOTIFY_USER");
            DropTable("dbo.MT_NOTIFY_TYPE");
            DropTable("dbo.MT_NOTIFY_MSG");
            DropTable("dbo.NOTIFY_GROUP");
        }
    }
}
