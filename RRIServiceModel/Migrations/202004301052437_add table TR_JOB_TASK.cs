﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_JOB_TASK : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_JOB_TASK",
                c => new
                    {
                        JOB_TASK_ID = c.Int(nullable: false, identity: true),
                        JOB_NO = c.String(maxLength: 20),
                        PROJECT_H_ID = c.Int(nullable: false),
                        PARENT_TASK_CODE = c.String(maxLength: 11),
                        TASK_GROUP_ID = c.Int(nullable: false),
                        PRIORITY_ID = c.Int(nullable: false),
                        ACTUAL_START_DATE = c.DateTime(),
                        ACTUAL_END_DATE = c.DateTime(),
                        PLAN_START_DATE = c.DateTime(),
                        PLAN_END_DATE = c.DateTime(),
                        DUE_DATE = c.DateTime(),
                        NOTE = c.String(),
                        PROGRESS_STATUS_ID = c.Int(nullable: false),
                        DEPT_CODE = c.String(maxLength: 3),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.JOB_TASK_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_JOB_TASK");
        }
    }
}
