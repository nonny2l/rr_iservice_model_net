﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changelengthtask_code2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TR_JOB_TASK", "PARENT_TASK_CODE", c => c.String(maxLength: 20));
            AlterColumn("dbo.TR_TASK_DOCUMENT_COMMENT", "TASK_CODE", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.TR_TIME_SHEET", "TASK_CODE", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TR_TIME_SHEET", "TASK_CODE", c => c.String(maxLength: 11));
            AlterColumn("dbo.TR_TASK_DOCUMENT_COMMENT", "TASK_CODE", c => c.String(nullable: false, maxLength: 11));
            AlterColumn("dbo.TR_JOB_TASK", "PARENT_TASK_CODE", c => c.String(maxLength: 11));
        }
    }
}
