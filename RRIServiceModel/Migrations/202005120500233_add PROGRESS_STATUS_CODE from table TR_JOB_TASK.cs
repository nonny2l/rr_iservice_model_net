﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPROGRESS_STATUS_CODEfromtableTR_JOB_TASK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB_TASK", "PROGRESS_STATUS_CODE", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_JOB_TASK", "PROGRESS_STATUS_CODE");
        }
    }
}
