﻿// <auto-generated />
namespace RRPlatFormModel
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class addtableTR_TEAMandTR_TEAM_ROLE : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addtableTR_TEAMandTR_TEAM_ROLE));
        
        string IMigrationMetadata.Id
        {
            get { return "202005201001073_add table TR_TEAM and TR_TEAM_ROLE"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
