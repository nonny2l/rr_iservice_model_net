﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcollumnTASK_JOB_CODEinTR_TEAMandTR_JOB_TASK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB_TASK", "TASK_JOB_CODE", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.TR_TEAM", "TASK_JOB_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_TEAM", "JOB_TARK_CODE", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_TEAM", "JOB_TARK_CODE");
            DropColumn("dbo.TR_TEAM", "TASK_JOB_CODE");
            DropColumn("dbo.TR_JOB_TASK", "TASK_JOB_CODE");
        }
    }
}
