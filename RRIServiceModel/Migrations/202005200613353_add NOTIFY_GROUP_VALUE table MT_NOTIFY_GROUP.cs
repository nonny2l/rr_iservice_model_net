﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNOTIFY_GROUP_VALUEtableMT_NOTIFY_GROUP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_NOTIFY_GROUP", "NOTIFY_GROUP_VALUE", c => c.String(maxLength: 100));
            AlterColumn("dbo.MT_NOTIFY_GROUP", "NOTIFY_GROUP_CODE", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MT_NOTIFY_GROUP", "NOTIFY_GROUP_CODE", c => c.String(maxLength: 100));
            DropColumn("dbo.MT_NOTIFY_GROUP", "NOTIFY_GROUP_VALUE");
        }
    }
}
