﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtaskNameTR_RASK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_TASK", "TASK_NAME", c => c.String(maxLength: 300));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_TASK", "TASK_NAME");
        }
    }
}
