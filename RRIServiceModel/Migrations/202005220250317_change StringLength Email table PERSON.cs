﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeStringLengthEmailtablePERSON : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MT_PERSON", "EMAIL", c => c.String(maxLength: 255));
            AlterColumn("dbo.TR_COMPANY_PERSON", "EMAIL", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TR_COMPANY_PERSON", "EMAIL", c => c.String(maxLength: 100));
            AlterColumn("dbo.MT_PERSON", "EMAIL", c => c.String(maxLength: 20));
        }
    }
}
