﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changejobtaskallidascode : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MT_PROJECT_D");
            DropPrimaryKey("dbo.MT_TASK_GROUP");
            CreateTable(
                "dbo.MT_BUDGET_UNIT",
                c => new
                    {
                        BUDGET_UNIT_ID = c.Int(nullable: false, identity: true),
                        BUDGET_UNIT_CODE = c.String(maxLength: 20),
                        BUDGET_UNIT_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.BUDGET_UNIT_ID);
            
            AddColumn("dbo.MT_PROJECT_D", "PROJECT_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_PROJECT_D", "SERVICE_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_PROJECT_D", "SUB_SERVICE_CODE", c => c.String(nullable: false, maxLength: 3));
            AddColumn("dbo.MT_PROJECT_D", "BRIEF_DATE", c => c.DateTime(nullable: false));
            AddColumn("dbo.MT_PROJECT_H", "BUDGET_UNIT_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.MT_TASK_GROUP", "TASK_GROUP_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_TASK_GROUP", "TASK_GROUP_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.MT_TASK_GROUP", "TASK_GROUP_NAME", c => c.String(maxLength: 100));
            AddColumn("dbo.MT_TASK_STATUS", "STATUS_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_JOB_TASK", "PROJECT_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.TR_JOB_TASK", "TASK_GROUP_CODE", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.TR_JOB_TASK", "PRIORITY_CODE", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.TR_JOB_TASK", "JOB_STATUS_CODE", c => c.String(maxLength: 100));
            AddPrimaryKey("dbo.MT_PROJECT_D", new[] { "PROJECT_D_ID", "PROJECT_CODE", "SERVICE_CODE", "SUB_SERVICE_CODE", "PROD_TYPE_CODE" });
            AddPrimaryKey("dbo.MT_TASK_GROUP", "TASK_GROUP_ID");
            DropColumn("dbo.MT_PROJECT_D", "PROJECT_H_ID");
            DropColumn("dbo.MT_PROJECT_D", "SERVICE_ID");
            DropColumn("dbo.MT_PROJECT_D", "SUB_SERVICE_ID");
            DropColumn("dbo.MT_PROJECT_H", "BUDGET_UNIT_TYPE_ID");
            DropColumn("dbo.MT_TASK_GROUP", "GROUP_ID");
            DropColumn("dbo.MT_TASK_GROUP", "GROUP_NAME");
            DropColumn("dbo.TR_JOB_TASK", "PROJECT_H_ID");
            DropColumn("dbo.TR_JOB_TASK", "TASK_GROUP_ID");
            DropColumn("dbo.TR_JOB_TASK", "PRIORITY_ID");
            DropColumn("dbo.TR_JOB_TASK", "PROGRESS_STATUS_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_JOB_TASK", "PROGRESS_STATUS_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_JOB_TASK", "PRIORITY_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_JOB_TASK", "TASK_GROUP_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_JOB_TASK", "PROJECT_H_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_TASK_GROUP", "GROUP_NAME", c => c.String(maxLength: 100));
            AddColumn("dbo.MT_TASK_GROUP", "GROUP_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MT_PROJECT_H", "BUDGET_UNIT_TYPE_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_PROJECT_D", "SUB_SERVICE_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_PROJECT_D", "SERVICE_ID", c => c.Int(nullable: false));
            AddColumn("dbo.MT_PROJECT_D", "PROJECT_H_ID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.MT_TASK_GROUP");
            DropPrimaryKey("dbo.MT_PROJECT_D");
            DropColumn("dbo.TR_JOB_TASK", "JOB_STATUS_CODE");
            DropColumn("dbo.TR_JOB_TASK", "PRIORITY_CODE");
            DropColumn("dbo.TR_JOB_TASK", "TASK_GROUP_CODE");
            DropColumn("dbo.TR_JOB_TASK", "PROJECT_CODE");
            DropColumn("dbo.MT_TASK_STATUS", "STATUS_CODE");
            DropColumn("dbo.MT_TASK_GROUP", "TASK_GROUP_NAME");
            DropColumn("dbo.MT_TASK_GROUP", "TASK_GROUP_CODE");
            DropColumn("dbo.MT_TASK_GROUP", "TASK_GROUP_ID");
            DropColumn("dbo.MT_PROJECT_H", "BUDGET_UNIT_CODE");
            DropColumn("dbo.MT_PROJECT_D", "BRIEF_DATE");
            DropColumn("dbo.MT_PROJECT_D", "SUB_SERVICE_CODE");
            DropColumn("dbo.MT_PROJECT_D", "SERVICE_CODE");
            DropColumn("dbo.MT_PROJECT_D", "PROJECT_CODE");
            DropTable("dbo.MT_BUDGET_UNIT");
            AddPrimaryKey("dbo.MT_TASK_GROUP", "GROUP_ID");
            AddPrimaryKey("dbo.MT_PROJECT_D", new[] { "PROJECT_D_ID", "PROJECT_H_ID", "SERVICE_ID", "SUB_SERVICE_ID", "PROD_TYPE_CODE" });
        }
    }
}
