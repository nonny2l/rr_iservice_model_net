﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatenotifyall : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.NOTIFY_GROUP", newName: "TR_NOTIFY_GROUP_ROLE");
            RenameTable(name: "dbo.NOTIFY_USER", newName: "TR_NOTIFY_USER_ROLE");
            DropPrimaryKey("dbo.TR_COMPANY_PERSON");
            DropPrimaryKey("dbo.TR_NOTIFY_MSG");
            CreateTable(
                "dbo.MT_NOTIFY_CONFIG",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_CONFIG_CODE = c.String(maxLength: 20),
                        NOTIFY_TOPIC_CODE = c.String(maxLength: 20),
                        NOTIFY_GROUP_CODE = c.String(maxLength: 20),
                        NOTIFY_CONFIG_HEADER_TEMPLATE = c.String(maxLength: 100),
                        NOTIFY_CONFIG_DETAIL_TEMPLATE = c.String(maxLength: 255),
                        IMG_URL = c.String(maxLength: 255),
                        URL = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_NOTIFY_GROUP",
                c => new
                    {
                        NOTIFY_GROUP_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_GROUP_CODE = c.String(maxLength: 100),
                        NOTIFY_GROUP_NAME = c.String(maxLength: 100),
                        NOTIFY_GROUP_DESC = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTIFY_GROUP_ID);
            
            CreateTable(
                "dbo.MT_NOTIFY_TOPIC",
                c => new
                    {
                        NOTIFY_TOPIC_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_TOPIC_CODE = c.String(maxLength: 20),
                        NOTIFY_TOPIC_NAME = c.String(maxLength: 100),
                        NOTITY_TOPIC_DESC = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTIFY_TOPIC_ID);
            
            AddColumn("dbo.TR_NOTIFY_GROUP_ROLE", "NOTIFY_MSG_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_GROUP_ROLE", "NOTIFY_GROUP_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.MT_PERSON", "PERSON_CODE", c => c.String(maxLength: 10));
            AddColumn("dbo.MT_PERSON", "PRE_NAME_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_NOTIFY_USER_ROLE", "NOTIFY_MSG_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_USER_ROLE", "EMP_NO", c => c.String(maxLength: 10));
            AddColumn("dbo.TR_COMPANY_PERSON", "PERSON_CODE", c => c.String(nullable: false, maxLength: 10));
            AddColumn("dbo.TR_COMPANY_PERSON", "DEPT_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TOPIC_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_GROUP_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_HEADER", c => c.String(maxLength: 255));
            AddColumn("dbo.TR_NOTIFY_USER_READ", "NOTIFY_MSG_CODE", c => c.String(maxLength: 20));
            AddColumn("dbo.TR_NOTIFY_USER_READ", "EMP_NO", c => c.String(maxLength: 10));
            AddPrimaryKey("dbo.TR_COMPANY_PERSON", new[] { "COMP_PERSON_ID", "COMP_CODE", "PERSON_CODE" });
            AddPrimaryKey("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_ID");
            DropColumn("dbo.TR_NOTIFY_GROUP_ROLE", "NOTIFY_ID");
            DropColumn("dbo.TR_NOTIFY_GROUP_ROLE", "USER_GROUP_ID");
            DropColumn("dbo.MT_PERSON", "TITLE_NAME");
            DropColumn("dbo.TR_NOTIFY_USER_ROLE", "NOTIFY_ID");
            DropColumn("dbo.TR_NOTIFY_USER_ROLE", "USER_ID");
            DropColumn("dbo.TR_COMPANY_PERSON", "PERSON_ID");
            DropColumn("dbo.TR_COMPANY_PERSON", "DEPARTMENT");
            DropColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION");
            DropColumn("dbo.TR_COMPANY_PERSON", "CONTACT_TYPE_CODE");
            DropColumn("dbo.TR_COMPANY_PERSON", "PERSON_SOURCE_CODE");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_ID");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TYPE_ID");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_GROUP_ID");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TOPIC");
            DropColumn("dbo.TR_NOTIFY_MSG", "MSG_USER_CODE");
            DropColumn("dbo.TR_NOTIFY_MSG", "MSG_USER_TYPE");
            DropColumn("dbo.TR_NOTIFY_USER_READ", "NOTIFY_ID");
            DropColumn("dbo.TR_NOTIFY_USER_READ", "USER_ID");
            DropTable("dbo.MT_NOTIFY_MSG");
            DropTable("dbo.MT_NOTIFY_TYPE");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MT_NOTIFY_TYPE",
                c => new
                    {
                        NOTIFY_TYPE_ID = c.Int(nullable: false, identity: true),
                        NOTIFY_TYPE_NAME = c.String(maxLength: 255),
                        NOTITY_TYPE_DESC = c.String(maxLength: 255),
                        ACTION_TYPE = c.String(maxLength: 20),
                        ACTION_CONDDITION_TYPE = c.String(maxLength: 50),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTIFY_TYPE_ID);
            
            CreateTable(
                "dbo.MT_NOTIFY_MSG",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NOTIFY_TYPE_ID = c.Int(),
                        NOTIFY_GROUP_ID = c.Int(),
                        NOTIFY_TOPIC = c.String(maxLength: 255),
                        NOTIFY_DESC = c.String(maxLength: 255),
                        IMG_URL = c.String(maxLength: 255),
                        URL = c.String(maxLength: 255),
                        START_DATE = c.DateTime(),
                        END_DATE = c.DateTime(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.TR_NOTIFY_USER_READ", "USER_ID", c => c.Int());
            AddColumn("dbo.TR_NOTIFY_USER_READ", "NOTIFY_ID", c => c.Int());
            AddColumn("dbo.TR_NOTIFY_MSG", "MSG_USER_TYPE", c => c.String());
            AddColumn("dbo.TR_NOTIFY_MSG", "MSG_USER_CODE", c => c.String());
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TOPIC", c => c.String(maxLength: 255));
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_GROUP_ID", c => c.Int());
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TYPE_ID", c => c.Int());
            AddColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_ID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.TR_COMPANY_PERSON", "PERSON_SOURCE_CODE", c => c.String(maxLength: 60));
            AddColumn("dbo.TR_COMPANY_PERSON", "CONTACT_TYPE_CODE", c => c.String(maxLength: 60));
            AddColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_COMPANY_PERSON", "DEPARTMENT", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_COMPANY_PERSON", "PERSON_ID", c => c.Int(nullable: false));
            AddColumn("dbo.TR_NOTIFY_USER_ROLE", "USER_ID", c => c.Int());
            AddColumn("dbo.TR_NOTIFY_USER_ROLE", "NOTIFY_ID", c => c.Int());
            AddColumn("dbo.MT_PERSON", "TITLE_NAME", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.TR_NOTIFY_GROUP_ROLE", "USER_GROUP_ID", c => c.Int());
            AddColumn("dbo.TR_NOTIFY_GROUP_ROLE", "NOTIFY_ID", c => c.Int());
            DropPrimaryKey("dbo.TR_NOTIFY_MSG");
            DropPrimaryKey("dbo.TR_COMPANY_PERSON");
            DropColumn("dbo.TR_NOTIFY_USER_READ", "EMP_NO");
            DropColumn("dbo.TR_NOTIFY_USER_READ", "NOTIFY_MSG_CODE");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_HEADER");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_GROUP_CODE");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_TOPIC_CODE");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_CODE");
            DropColumn("dbo.TR_NOTIFY_MSG", "NOTIFY_MSG_ID");
            DropColumn("dbo.TR_COMPANY_PERSON", "JOB_POSITION_CODE");
            DropColumn("dbo.TR_COMPANY_PERSON", "DEPT_CODE");
            DropColumn("dbo.TR_COMPANY_PERSON", "PERSON_CODE");
            DropColumn("dbo.TR_NOTIFY_USER_ROLE", "EMP_NO");
            DropColumn("dbo.TR_NOTIFY_USER_ROLE", "NOTIFY_MSG_CODE");
            DropColumn("dbo.MT_PERSON", "PRE_NAME_CODE");
            DropColumn("dbo.MT_PERSON", "PERSON_CODE");
            DropColumn("dbo.TR_NOTIFY_GROUP_ROLE", "NOTIFY_GROUP_CODE");
            DropColumn("dbo.TR_NOTIFY_GROUP_ROLE", "NOTIFY_MSG_CODE");
            DropTable("dbo.MT_NOTIFY_TOPIC");
            DropTable("dbo.MT_NOTIFY_GROUP");
            DropTable("dbo.MT_NOTIFY_CONFIG");
            AddPrimaryKey("dbo.TR_NOTIFY_MSG", "NOTIFY_ID");
            AddPrimaryKey("dbo.TR_COMPANY_PERSON", new[] { "COMP_PERSON_ID", "PERSON_ID", "COMP_CODE" });
            RenameTable(name: "dbo.TR_NOTIFY_USER_ROLE", newName: "NOTIFY_USER");
            RenameTable(name: "dbo.TR_NOTIFY_GROUP_ROLE", newName: "NOTIFY_GROUP");
        }
    }
}
