﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_TIME_SHEET : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_TIME_SHEET",
                c => new
                    {
                        TIMESHEET_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        TASK_CODE = c.String(maxLength: 11),
                        START_DATE = c.DateTime(nullable: false),
                        END_DATE = c.DateTime(),
                        WORKING_TIME = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 20),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 20),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 20),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.TIMESHEET_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_TIME_SHEET");
        }
    }
}
