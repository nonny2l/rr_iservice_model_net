﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removefieldjob_tark_codeintr_team : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TR_TEAM", "JOB_TARK_CODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_TEAM", "JOB_TARK_CODE", c => c.String(maxLength: 20));
        }
    }
}
