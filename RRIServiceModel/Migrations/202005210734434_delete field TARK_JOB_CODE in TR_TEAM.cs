﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletefieldTARK_JOB_CODEinTR_TEAM : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TR_TEAM", "TASK_JOB_CODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_TEAM", "TASK_JOB_CODE", c => c.String(maxLength: 20));
        }
    }
}
