﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changelengthtask_code : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TR_TASK", "TASK_CODE", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TR_TASK", "TASK_CODE", c => c.String(nullable: false, maxLength: 11));
        }
    }
}
