﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cahngeestimate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB_TASK", "ESTIMATED", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_TASK", "ESTIMATED", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_TIME_SHEET", "HOURS_QTY", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_TIME_SHEET", "MINUTE_QTY", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.TR_JOB_TASK", "ESTIMATED_CODE");
            DropColumn("dbo.TR_TASK", "ESTIMATED_CODE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_TASK", "ESTIMATED_CODE", c => c.String(maxLength: 100));
            AddColumn("dbo.TR_JOB_TASK", "ESTIMATED_CODE", c => c.String(maxLength: 100));
            DropColumn("dbo.TR_TIME_SHEET", "MINUTE_QTY");
            DropColumn("dbo.TR_TIME_SHEET", "HOURS_QTY");
            DropColumn("dbo.TR_TASK", "ESTIMATED");
            DropColumn("dbo.TR_JOB_TASK", "ESTIMATED");
        }
    }
}
