﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_add_tr_noti : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_NOTI",
                c => new
                    {
                        NOTI_ID = c.Int(nullable: false, identity: true),
                        NOTI_NAME = c.String(),
                        NOTI_SUBJECT = c.String(),
                        NOTI_DESC = c.String(),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.NOTI_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_NOTI");
        }
    }
}
