﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcomcodeandupdatetimesheet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_JOB_TASK", "COMP_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.TR_TIME_SHEET", "TIME_SHEET_ACTIVE", c => c.String(maxLength: 1));
            DropColumn("dbo.TR_JOB_TASK", "PROGRESS_STATUS_CODE");
            DropColumn("dbo.TR_TIME_SHEET", "HOURS_QTY");
            DropColumn("dbo.TR_TIME_SHEET", "MINUTE_QTY");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TR_TIME_SHEET", "MINUTE_QTY", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_TIME_SHEET", "HOURS_QTY", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.TR_JOB_TASK", "PROGRESS_STATUS_CODE", c => c.String(maxLength: 20));
            DropColumn("dbo.TR_TIME_SHEET", "TIME_SHEET_ACTIVE");
            DropColumn("dbo.TR_JOB_TASK", "COMP_CODE");
        }
    }
}
