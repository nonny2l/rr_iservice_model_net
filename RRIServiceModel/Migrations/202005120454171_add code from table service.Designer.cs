﻿// <auto-generated />
namespace RRPlatFormModel
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class addcodefromtableservice : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addcodefromtableservice));
        
        string IMigrationMetadata.Id
        {
            get { return "202005120454171_add code from table service"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
