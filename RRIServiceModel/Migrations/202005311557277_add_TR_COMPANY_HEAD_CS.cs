﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_TR_COMPANY_HEAD_CS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_COMPANY_HEAD_CS",
                c => new
                    {
                        COMP_CODE = c.String(nullable: false, maxLength: 3),
                        EMP_NO = c.String(nullable: false, maxLength: 10),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.COMP_CODE, t.EMP_NO });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_COMPANY_HEAD_CS");
        }
    }
}
