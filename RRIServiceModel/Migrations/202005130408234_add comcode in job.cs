﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcomcodeinjob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_PROJECT_D", "COMP_CODE", c => c.String(maxLength: 3));
            AddColumn("dbo.MT_PROJECT_D", "DUE_DATE", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_PROJECT_D", "DUE_DATE");
            DropColumn("dbo.MT_PROJECT_D", "COMP_CODE");
        }
    }
}
