﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update20200603_1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_TEAM", "JOB_TASK_CODE", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_TEAM", "JOB_TASK_CODE");
        }
    }
}
