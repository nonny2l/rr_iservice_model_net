﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update20200528 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.TR_JOB");
            AlterColumn("dbo.TR_JOB", "JOB_STATUS_CODE", c => c.String(maxLength: 100));
            AddPrimaryKey("dbo.TR_JOB", new[] { "JOB_ID", "PROJECT_CODE", "SERVICE_CODE", "SUB_SERVICE_CODE" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.TR_JOB");
            AlterColumn("dbo.TR_JOB", "JOB_STATUS_CODE", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("dbo.TR_JOB", new[] { "JOB_ID", "PROJECT_CODE", "JOB_STATUS_CODE", "SERVICE_CODE", "SUB_SERVICE_CODE" });
        }
    }
}
