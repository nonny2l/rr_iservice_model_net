﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeserviceidtoservicecodeMT_SUB_SERVICE : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MT_SUB_SERVICE");
            AddColumn("dbo.MT_SUB_SERVICE", "SERVICE_CODE", c => c.String(nullable: false, maxLength: 3));
            AddPrimaryKey("dbo.MT_SUB_SERVICE", new[] { "SUB_SERVICE_ID", "SERVICE_CODE" });
            DropColumn("dbo.MT_SUB_SERVICE", "SERVICE_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MT_SUB_SERVICE", "SERVICE_ID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.MT_SUB_SERVICE");
            DropColumn("dbo.MT_SUB_SERVICE", "SERVICE_CODE");
            AddPrimaryKey("dbo.MT_SUB_SERVICE", new[] { "SUB_SERVICE_ID", "SERVICE_ID" });
        }
    }
}
