﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableMT_ORG_POSITION : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_ORG_POSITION",
                c => new
                    {
                        ORG_POSI_ID = c.Int(nullable: false, identity: true),
                        SEQ = c.Int(nullable: false),
                        DEPT_CODE = c.String(maxLength: 3),
                        POSITION_CODE = c.String(maxLength: 3),
                        POSITION_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                        CREATE_BY = c.String(nullable: false, maxLength: 255),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 255),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 255),
                        DELETE_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ORG_POSI_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_ORG_POSITION");
        }
    }
}
