﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addculumntaskcodeincomment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TR_TASK_DOCUMENT", "TASK_CODE", c => c.String(nullable: false, maxLength: 11));
            AddColumn("dbo.TR_TASK_DOCUMENT_COMMENT", "TASK_CODE", c => c.String(nullable: false, maxLength: 11));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TR_TASK_DOCUMENT_COMMENT", "TASK_CODE");
            DropColumn("dbo.TR_TASK_DOCUMENT", "TASK_CODE");
        }
    }
}
