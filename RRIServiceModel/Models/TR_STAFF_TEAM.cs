﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_STAFF_TEAM : MasterColumnsModel
    {
        [Key]
        public int STAFF_TEAM_ID { get; set; }
        public int USER_ID { get; set; }
        public int? PARENT_ID { get; set; }
        public string USER_NAME { get; set; }
    }
}
