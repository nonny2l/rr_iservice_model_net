using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_NOTIFY_GROUP_ROLE : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(20)]
        public string NOTIFY_MSG_CODE { get; set; }

        [StringLength(100)]
        public string NOTIFY_GROUP_CODE { get; set; }
    }
}
