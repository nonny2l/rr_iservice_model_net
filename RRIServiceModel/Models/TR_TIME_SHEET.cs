﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TIME_SHEET : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TIME_SHEET_ID { get; set; }

        [StringLength(10)]
        public string EMP_NO { get; set; }

        [StringLength(20)]
        public string TASK_CODE { get; set; }

        public DateTime START_DATE { get; set; }

        public DateTime? END_DATE { get; set; }

        public decimal WORK_TIME { get; set; }

        [StringLength(1)]
        public string TIME_SHEET_ACTIVE { get; set; }
    }
}
