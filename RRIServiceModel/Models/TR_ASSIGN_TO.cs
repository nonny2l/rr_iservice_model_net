﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_ASSIGN_TO : MasterColumnsModel
    {

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ASSIGN_TO_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EMP_NO { get; set; }

        [Column(Order = 2)]
        public string EMAIL { get; set; }

        [Key]
        [Column(Order = 3)]
        public string TASK_CODE { get; set; }
        
    }
}
