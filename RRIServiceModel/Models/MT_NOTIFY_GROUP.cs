﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{

    public partial class MT_NOTIFY_GROUP : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NOTIFY_GROUP_ID { get; set; }

        [StringLength(20)]
        public string NOTIFY_GROUP_CODE { get; set; }

        [StringLength(100)]
        public string NOTIFY_GROUP_VALUE { get; set; }

        [StringLength(100)]
        public string NOTIFY_GROUP_NAME { get; set; }

        [StringLength(255)]
        public string NOTIFY_GROUP_DESC { get; set; }


    }
}
