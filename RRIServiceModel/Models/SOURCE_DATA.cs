﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SOURCE_DATA : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int CUSTOMER_ID { get; set; }

        [StringLength(1000)]
        public string SOURCE_NAME { get; set; }

    }
}
