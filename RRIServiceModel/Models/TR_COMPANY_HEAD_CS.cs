﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_COMPANY_HEAD_CS : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string COMP_CODE { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EMP_NO { get; set; }
    }
}
