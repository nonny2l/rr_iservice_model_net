﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_JOB_ATTACHMENT : MasterColumnsModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int JOB_ATTACHMENT_ID { get; set; }

        public int SEQ { get; set; }

        [StringLength(20)]
        public string JOB_NO { get; set; }

        [Column(TypeName = "text")]
        public string FILE_PATH { get; set; }

        [Column(TypeName = "text")]
        public string FILE_NAME { get; set; }

        [Column(TypeName = "text")]
        public string OLD_FILE_NAME { get; set; }

        [Column(TypeName = "text")]
        public string FILE_TYPE { get; set; }

    }
}
