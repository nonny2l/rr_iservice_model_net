﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_CONFIG_D : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CONFIG_D_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string CONFIG_H_CODE { get; set; }
        [StringLength(60)]
        public string CONFIG_D_CODE { get; set; }
        public int CONFIG_D_SEQ { get; set; }
        public string CONFIG_D_TNAME { get; set; }
        public string CONFIG_D_ENAME { get; set; }
        public int? PARENT_D_ID { get; set; }
    }
}
