﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MasterColumnsModel
    {
     
        [StringLength(1)]
        [Required]
        public string IS_ACTIVE { get; set; }

        [StringLength(255)]
        [Required]
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }

        [StringLength(255)]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }

        [StringLength(255)]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }
    }
}
