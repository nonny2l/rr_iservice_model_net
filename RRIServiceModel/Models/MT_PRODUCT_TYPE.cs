﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_PRODUCT_TYPE : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int PROD_TYPE_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string PROD_TYPE_CODE { get; set; }
        [StringLength(100)]
        public string PROD_TYPE_NAME { get; set; }
    }
}
