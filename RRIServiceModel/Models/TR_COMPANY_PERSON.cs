﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_COMPANY_PERSON : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int COMP_PERSON_ID { get; set; }

        [StringLength(3)]
        [Key]
        [Column(Order = 2)]
        public string COMP_CODE { get; set; }

        [StringLength(10)]
        [Key]
        [Column(Order = 3)]
        public string PERSON_CODE { get; set; }

        //[StringLength(3)]
        //public string DEPT_CODE { get; set; }

        

        [StringLength(255)]
        public string EMAIL { get; set; }
        [StringLength(100)]
        public string MOBILE { get; set; }

        [StringLength(1)]
        public string IS_MAIN { get; set; }
        [StringLength(100)]
        public string JOB_POSITION { get; set; }
    }
}
