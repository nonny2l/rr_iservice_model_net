﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_DISTRICT : MasterColumnsModel
    {
        [Key]
        [StringLength(4)]
        public string DISTRICT_CODE { get; set; }
        [Required]
        [StringLength(200)]
        public string DISTRICT_TNAME { get; set; }
        [StringLength(200)]
        public string DISTRICT_ENAME { get; set; }
        [Required]
        [StringLength(2)]
        public string REF_PROV_CODE { get; set; }
    }
}
