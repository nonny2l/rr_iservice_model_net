﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TASK_DOCUMENT : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int DOCUMENT_ID { get; set; }

        [StringLength(100)]
        public string DOCUMENT_NAME { get; set; }

        [StringLength(200)]
        public string DOCUMENT_URL { get; set; }

        [StringLength(11)]
        [Required]
        public string TASK_CODE { get; set; }
    }
}
