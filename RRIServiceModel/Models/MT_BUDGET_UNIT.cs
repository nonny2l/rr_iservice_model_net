﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{

    public partial class MT_BUDGET_UNIT : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BUDGET_UNIT_ID { get; set; }

        [StringLength(20)]
        public string BUDGET_UNIT_CODE { get; set; }

        [StringLength(255)]
        public string BUDGET_UNIT_NAME { get; set; }

    }

}
