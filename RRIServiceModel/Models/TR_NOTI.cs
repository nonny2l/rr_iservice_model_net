﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_NOTI : MasterColumnsModel
    {
        [Key]
        public int NOTI_ID { get; set; }
        public string NOTI_NAME { get; set; }
        public string NOTI_SUBJECT { get; set; }
        public string NOTI_DESC { get; set; }
    }
}
