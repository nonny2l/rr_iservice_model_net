﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_DEPARTMENT : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int DEPT_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string DEPT_CODE { get; set; }
        [StringLength(255)]
        public string DEPT_NAME { get; set; }
        public int SEQ { get; set; }
        //[StringLength(255)]
        //public string JOB_HNDL_FLAG { get; set; }
    }
}
