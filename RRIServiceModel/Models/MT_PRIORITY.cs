﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_PRIORITY : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int PRIORITY_ID { get; set; }

        [StringLength(100)]
        public string PRIORITY_CODE { get; set; }

        [StringLength(200)]
        public string PRIORITY_NAME { get; set; }

        [StringLength(4000)]
        public string REMARK { get; set; }

        
    }
}
