﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TASK_DOCUMENT_COMMENT : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int COMMENT_ID { get; set; }

        [StringLength(200)]
        public string COMMENT_DESC { get; set; }

        [StringLength(20)]
        [Required]
        public string TASK_CODE { get; set; }

        public int USER_ID { get; set; }
        
    }
}
