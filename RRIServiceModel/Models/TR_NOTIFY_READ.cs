using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_NOTIFY_READ : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TR_NOTIFY__READ_ID { get; set; }

        [StringLength(100)]
        [Required]
        public string NOTIFY_MSG_CODE { get; set; }

        [StringLength(10)]
        [Required]
        public string EMP_NO { get; set; }
    }
}
