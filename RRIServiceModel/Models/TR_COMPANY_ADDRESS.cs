﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_COMPANY_ADDRESS : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int COMP_ADDR_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string COMP_CODE { get; set; }
        [StringLength(255)]
        public string ADDRESS { get; set; }
        [StringLength(4)]
        public string DISTRICT_CODE { get; set; }
        [StringLength(6)]
        public string SUB_DISTRICT_CODE { get; set; }
        [StringLength(2)]
        public string PROV_CODE { get; set; }
        [StringLength(5)]
        public string ZIPCODE { get; set; }
        [StringLength(20)]
        public string ZONE { get; set; }
        [StringLength(1)]
        [Required]
        public string IS_MAIN { get; set; }

    }
}
