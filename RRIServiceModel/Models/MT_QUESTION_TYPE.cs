﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_QUESTION_TYPE : MasterColumnsModel
    {
        [Key]
        public int QUESTION_TYPE_ID { get; set; }
        [StringLength(255)]
        [Required]
        public string QUESTION_TYPE_DESC { get; set; }
        [StringLength(1)]
        [Required]
        public string QUESTION_GROUP { get; set; }
        public int SEQ { get; set; }

    }
}
