﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_TASK_GROUP : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TASK_GROUP_ID { get; set; }

        [Column(Order = 1)]
        [StringLength(20)]
        public string TASK_GROUP_CODE { get; set; }

        [Column(Order = 2)]
        [StringLength(100)]
        public string TASK_GROUP_NAME { get; set; }

        public int SEQ { get; set; }

    }
}
