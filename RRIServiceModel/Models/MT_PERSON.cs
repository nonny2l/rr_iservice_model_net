﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_PERSON : MasterColumnsModel
    {
        [Key]
        public int PERSON_ID { get; set; }

        [StringLength(10)]
        public string PERSON_CODE { get; set; }

        [StringLength(100)]
        public string PRE_NAME_CODE { get; set; }

        [Required]
        [StringLength(100)]
        public string FIRST_NAME { get; set; }
        [Required]
        [StringLength(100)]
        public string LAST_NAME { get; set; }
        [StringLength(255)]
        public string TELEPHONE { get; set; }
        [StringLength(255)]
        public string EMAIL { get; set; }
        [StringLength(20)]
        public string MOBILE { get; set; }
        [StringLength(13)]
        public string CITIZEN_ID { get; set; }
        [StringLength(20)]
        public string PASSPORT { get; set; }
        [StringLength(255)]
        public string LINE_ID { get; set; }
    }
}
