﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_SOURCE_DATA_CATAGORY : MasterColumnsModel
    {
        [Key]
        public long ID { get; set; }
        [StringLength(255)]
        public string CATEGORY_NAME { get; set; }
    }
}
