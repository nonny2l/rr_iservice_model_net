﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_JOB : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int JOB_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string PROJECT_CODE { get; set; }

        [Column(Order = 2)]
        [StringLength(100)]
        public string JOB_STATUS_CODE { get; set; }

        [Required]
        [StringLength(20)]
        public string JOB_NO { get; set; }
        [Column(TypeName = "text")]
        public string JOB_DESC { get; set; }
        //[Required]
        //public DateTime STATUS_DATE { get; set; }
        [Key]
        [Column(Order = 3)]
        [StringLength(3)]
        public string SERVICE_CODE { get; set; }
        [Key]
        [Column(Order = 4)]
        [StringLength(3)]
        public string SUB_SERVICE_CODE { get; set; }
       
        [Required]
        public int WORK_BUDGET { get; set; }
        public DateTime DUE_DATE { get; set; }
        public DateTime BRIEF_DATE { get; set; }
        [StringLength(255)]
        public string ISSUE_BY { get; set; }
        public DateTime? ISSUE_DATE { get; set; }
        [StringLength(3)]
        [Required]
        public string COMP_CODE { get; set; }

        [StringLength(10)]
        public string PERSON_CODE { get; set; }

        [StringLength(100)]
        public string JOB_NAME { get; set; }

        [StringLength(4)]
        [Required]
        public string JOB_YEAR { get; set; }

        //[Required]
        //public DateTime JOB_DATE { get; set; }

        //[Key]
        //[Column(Order = 4)]
        //[StringLength(3)]
        //public string PROD_TYPE_CODE { get; set; }



        //[Column(TypeName = "text")]
        //public string CANCEL_REASON { get; set; }
        //[Column(TypeName = "text")]
        //public string PE_REMK { get; set; }
        //public int? PE_VAT_PCT { get; set; }
        //[StringLength(1)]
        //public string PE_VAT_FLAG { get; set; }

        //[Required]
        //public DateTime START_DATE { get; set; }
        //public DateTime? FINISH_DATE { get; set; }
        //public DateTime? ACTUAL_START_DATE { get; set; }
        //public DateTime? ACTUAL_FINISH_DATE { get; set; }

        //[StringLength(255)]
        //public string ACK_BY { get; set; }
        //public DateTime? ACK_DATE { get; set; }
        //[StringLength(255)]
        //public string APPV_BY { get; set; }
        //public DateTime? APPV_DATE { get; set; }
        //public int? ASSIGN_TO { get; set; }
        //public DateTime? ASSIGN_DATE { get; set; }
        //[StringLength(1)]
        //public string FREEZE_FLAG { get; set; }
        //[StringLength(4)]
        //public string FREEZE_YYMM { get; set; }
        //[StringLength(255)]
        //public string CLIENT_APPV_BY { get; set; }
        //public DateTime? CLIENT_APPV_DATE { get; set; }
        //public int? PE_AGCY_FEE_PCT { get; set; }
        //public int? LAST_VER_NO { get; set; }
        //public DateTime? LAST_VER_DATE { get; set; }
        //[StringLength(1)]
        //public string VER_LOCK { get; set; }
        //public int PE_AGCY_FEE_AMT { get; set; }
        //[Column(TypeName = "text")]
        //public string REMARK { get; set; }


    }
}
