﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_CONTACT_TYPE : MasterColumnsModel
    {
        [Key]
        [StringLength(2)]
        public string CONTACT_TYPE_CODE { get; set; }
        [StringLength(255)]
        public string CONTACT_TYPE_NAME { get; set; }
    }
}
