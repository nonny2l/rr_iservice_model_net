﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_PROVINCE : MasterColumnsModel
    {
        [Key]
        [StringLength(2)]
        public string PROV_CODE { get; set; }
        [Required]
        [StringLength(100)]
        public string PROV_TNAME { get; set; }
        [StringLength(100)]
        public string PROV_ENAME { get; set; }
        [Required]
        [StringLength(50)]
        public string PROV_REGION { get; set; }
    }
}
