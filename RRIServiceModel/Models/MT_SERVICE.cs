﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_SERVICE : MasterColumnsModel
    {
        [Key]
        public int SERVICE_ID { get; set; }

        [StringLength(3)]
        [Required]
        public string SERVICE_CODE { get; set; }

        [StringLength(100)]
        [Required]
        public string SERVICE_NAME { get; set; }

        public int SEQ { get; set; }
    }
}
