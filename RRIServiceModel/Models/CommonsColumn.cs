﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public class CommonsColumn
    {
        [StringLength(20)]
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        [StringLength(20)]
        public string UPDATE_BY { get; set; }
        public DateTime? UPDATE_DATE { get; set; }
        [StringLength(20)]
        public string DELETE_BY { get; set; }
        public DateTime? DELETE_DATE { get; set; }

    }
}
