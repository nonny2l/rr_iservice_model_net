﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_SUB_SERVICE : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int SUB_SERVICE_ID { get; set; }

        [StringLength(3)]
        [Required]
        public string SUB_SERVICE_CODE { get; set; }

        [StringLength(100)]
        [Required]
        public string SUB_SERVICE_NAME { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string SERVICE_CODE { get; set; }
    }
}
