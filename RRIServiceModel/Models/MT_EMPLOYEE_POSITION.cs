﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_EMPLOYEE_POSITION : MasterColumnsModel
    {

        [Key]
        public int EMP_POSITION_ID { get; set; }
        [StringLength(10)]
        [Required]
        public string EMP_NO { get; set; }
        public int SEQ { get; set; }
        [Required]
        [StringLength(3)]
        public string DEPT_CODE { get; set; }
        [Required]
        [StringLength(3)]
        public string POSITION_CODE { get; set; }
    }
}
