﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_TEAM_ROLE : MasterColumnsModel
    {
        [Key]
        public int TEAM_ROLE_ID { get; set; }
        [StringLength(10)]
        public string EMP_NO { get; set; }
        [StringLength(10)]
        public string PARENT_EMP_NO { get; set; }
        [StringLength(100)]
        public string ROLE_NAME { get; set; }
        [StringLength(20)]
        public string DEPT_CODE { get; set; }
    }
}
