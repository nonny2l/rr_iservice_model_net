﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{

    public partial class MT_NOTIFY_TOPIC : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NOTIFY_TOPIC_ID { get; set; }

        [StringLength(20)]
        [Required]
        public string NOTIFY_TOPIC_CODE { get; set; }

        [StringLength(100)]
        [Required]
        public string NOTIFY_TOPIC_NAME { get; set; }

        [StringLength(255)]
        public string NOTITY_TOPIC_DESC { get; set; }


    }
}
