﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_JOB_TASK : MasterColumnsModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int JOB_TASK_ID { get; set; }

        [StringLength(255)]
        [Required]
        public string JOB_TASK_NAME { get; set; }

        [StringLength(100)]
        [Required]
        public string JOB_NO { get; set; }

        [StringLength(3)]
        [Required]
        public string PROJECT_CODE { get; set; }

        [StringLength(20)]
        public string PARENT_TASK_CODE { get; set; }

        [Required]
        [StringLength(20)]
        public string TASK_GROUP_CODE { get; set; }

        [Required]
        [StringLength(100)]
        public string PRIORITY_CODE { get; set; }

        public DateTime? ACTUAL_START_DATE { get; set; }

        public DateTime? ACTUAL_END_DATE { get; set; }

        public DateTime? PLAN_START_DATE { get; set; }

        public DateTime? PLAN_END_DATE { get; set; }

        public DateTime? DUE_DATE { get; set; }

        [Column(TypeName = "text")]
        public string NOTE { get; set; }

        [StringLength(20)]
        [Required]
        public string JOB_TASK_STATUS_CODE { get; set; }

        [StringLength(3)]
        [Required]
        public string DEPT_CODE { get; set; }


        [StringLength(3)]
        [Required]
        public string COMP_CODE { get; set; }

        [Required]
        [StringLength(20)]
        public string JOB_TASK_CODE { get; set; }

        [StringLength(255)]
        public string ACCEPT_BY { get; set; }

        public DateTime? ACCEPT_DATE { get; set; }

        [StringLength(1)]
        public string ACCEPT_STATUS { get; set; }
      

    }
}
