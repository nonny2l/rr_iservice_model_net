using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_NOTIFY_CONFIG : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NOTIFY_CONFIG_ID { get; set; }

        [StringLength(20)]
        [Required]
        public string NOTIFY_CONFIG_CODE { get; set; }

        [StringLength(20)]
        [Required]
        public string NOTIFY_TOPIC_CODE { get; set; }

        [StringLength(20)]
        [Required]
        public string NOTIFY_GROUP_CODE { get; set; }

        [StringLength(100)]
        [Required]
        public string NOTIFY_CONFIG_HEADER_TEMPLATE { get; set; }

        [StringLength(255)]
        [Required]
        public string NOTIFY_CONFIG_DETAIL_TEMPLATE { get; set; }

        [StringLength(255)]
        public string IMG_URL { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

    }
}
