using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class TR_NOTIFY_MSG : MasterColumnsModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NOTIFY_MSG_ID { get; set; }

        [StringLength(100)]
        [Required]
        public string NOTIFY_MSG_CODE { get; set; }

        [StringLength(10)]
        [Required]
        public string EMP_NO { get; set; }

        [StringLength(20)]
        [Required]
        public string NOTIFY_TOPIC_CODE { get; set; }

        [StringLength(20)]
        [Required]
        public string NOTIFY_GROUP_CODE { get; set; }

        [StringLength(100)]
        [Required]
        public string NOTIFY_HEADER { get; set; }

        [StringLength(255)]
        [Required]
        public string NOTIFY_DESC { get; set; }

        [StringLength(255)]
        public string IMG_URL { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        [StringLength(100)]

        public string REF_TABLE { get; set; }
        [StringLength(100)]
        public string REF_KEY { get; set; }
        [StringLength(100)]
        public string REF_VALUE { get; set; }







    }
}
