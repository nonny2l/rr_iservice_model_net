﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TEAM : MasterColumnsModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int TEAM_ID { get; set; }

        [StringLength(100)]
        public string TEAM_CODE { get; set; }

        [StringLength(3)]
        public string DEPT_CODE { get; set; }

        [StringLength(20)]
        public string JOB_NO { get; set; }

        [StringLength(255)]
        public string TEAM_NAME { get; set; }

        [StringLength(20)]
        [Required]
        public string JOB_TASK_CODE { get; set; }
    }
}
