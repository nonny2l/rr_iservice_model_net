namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_MENU_TYPE : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MENU_TYPE_ID { get; set; }

        [StringLength(255)]
        public string MENU_TYPE_NAME { get; set; }
    }
}
