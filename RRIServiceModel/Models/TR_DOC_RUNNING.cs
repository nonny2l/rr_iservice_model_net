﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_DOC_RUNNING : MasterColumnsModel
    {

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DOCUMENT_RUNNING_ID { get; set; }

        public string COM_CODE { get; set; }

        public string DEPT_CODE { get; set; }

        public string PREFIX_CODE { get; set; }

        public int PREFIX_YEAR { get; set; }

        public int PREFIX_MONTH { get; set; }

        public int DOCUMENT_RUNNING { get; set; }

    }
}
