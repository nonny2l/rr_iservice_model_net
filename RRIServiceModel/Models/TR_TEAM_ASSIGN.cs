﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TEAM_ASSIGN : MasterColumnsModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int TEAM_ASSIGN_ID { get; set; }

        [StringLength(100)]
        public string TEAM_CODE { get; set; }

        [StringLength(10)]
        public string EMP_NO { get; set; }

        [StringLength(1)]
        public string IS_MAIN { get; set; }
        
    }
}
