﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_SUBDISTRICT : MasterColumnsModel
    {
        [Key]
        [StringLength(6)]
        public string SUB_DISTRICT_CODE { get; set; }
        [Required]
        [StringLength(200)]
        public string SUB_DISTRICT_TNAME { get; set; }
        [StringLength(200)]
        public string SUB_DISTRICT_ENAME { get; set; }
        [Required]
        [StringLength(4)]
        public string REF_DISTRICT_CODE { get; set; }
        [Required]
        [StringLength(5)]
        public string ZIPCODE { get; set; }
    }
}
