﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_PROJECT : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PROJECT_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string PROJECT_CODE { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(3)]
        public string COMP_CODE { get; set; }

        [Required]
        [StringLength(100)]
        public string PROJECT_NAME { get; set; }

        [StringLength(255)]
        [Column(TypeName = "text")]
        public string PROJECT_DESC { get; set; }

        public int BUDGET_VALUE { get; set; }

        [StringLength(20)]
        public string BUDGET_UNIT_CODE { get; set; }
    }
}
