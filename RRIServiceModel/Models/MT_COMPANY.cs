﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_COMPANY : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        public int COMP_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string COMP_CODE { get; set; }
        [StringLength(100)]
        [Required]
        public string COMP_NAME { get; set; }
        [StringLength(200)]
        public string BUSINESS_TYPE { get; set; }
        [StringLength(200)]
        public string SUB_BUSINESS_TYPE { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        [StringLength(20)]
        public string TAX_ID { get; set; }
        [StringLength(100)]
        public string TEL_NO { get; set; }
        [StringLength(100)]
        public string FAX_NO { get; set; }
        public int? TERM_OF_PAYMENT { get; set; }
        public int? AGENCY_FEE { get; set; }
        [StringLength(150)]
        public string FACEBOOK_ID { get; set; }
        [StringLength(255)]
        public string WEBSITE { get; set; }
        [StringLength(1)]
        [Required]
        public string IS_COMPANY { get; set; }
        [Column(TypeName = "text")]
        public string REMARK { get; set; }

    }
}
