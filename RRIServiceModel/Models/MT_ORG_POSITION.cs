﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RRPlatFormModel.Models
{
    public partial class MT_ORG_POSITION : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ORG_POSI_ID { get; set; }

        public int SEQ { get; set; }

        [StringLength(3)]
        public string DEPT_CODE { get; set; }

        [StringLength(3)]
        public string POSITION_CODE { get; set; }
    }
}
