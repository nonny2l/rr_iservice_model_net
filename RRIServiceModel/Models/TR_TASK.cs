﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TASK : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int TASK_ID { get; set; }
        [StringLength(20)]
        [Required]
        public string TASK_CODE { get; set; }

        [StringLength(255)]
        [Required]
        public string TASK_NAME { get; set; }

        [StringLength(20)]
        [Required]
        public string JOB_NO { get; set; }

        [StringLength(3)]
        [Required]
        public string PROJECT_CODE { get; set; }
        //[StringLength(11)]
        //public string PARENT_TASK_CODE { get; set; }
        [StringLength(3)]
        [Required]
        public string TASK_GROUP_CODE { get; set; }
        [Required]
        [StringLength(3)]
        public string PRIORITY_CODE { get; set; }
        //public DateTime? ACTUAL_START_DATE { get; set; }
        //public DateTime? ACTUAL_END_DATE { get; set; }
        public DateTime? PLAN_START_DATE { get; set; }
        public DateTime? PLAN_END_DATE { get; set; }
        public DateTime? DUE_DATE { get; set; }
        [Column(TypeName = "text")]
        public string NOTE { get; set; }
        //[Column(TypeName = "text")]
        //public string HASHTAG { get; set; }
        [Required]
        public decimal ESTIMATED { get; set; }
        [StringLength(20)]
        public string TASK_STATUS_CODE { get; set; }
        [StringLength(3)]
        [Required]
        public string COMP_CODE { get; set; }
    }
}
