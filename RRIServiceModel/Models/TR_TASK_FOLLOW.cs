﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class TR_TASK_FOLLOW : MasterColumnsModel
    {
        [Key]
        public int TASK_FOLLOW_ID { get; set; }
        [StringLength(10)]
        public string EMP_NO { get; set; }
        [StringLength(20)]
        public string TASK_CODE { get; set; }
    }
}
