﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_CONFIG_H : MasterColumnsModel
    {
        [Key]
        public int CONFIG_H_ID { get; set; }
        [StringLength(20)]
        public string CONFIG_H_CODE { get; set; }
        [StringLength(100)]
        public string CONFIG_H_TNAME { get; set; }
        [StringLength(100)]
        public string CONFIG_H_ENAME { get; set; }

    }
}
