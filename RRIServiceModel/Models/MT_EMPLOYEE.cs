﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_EMPLOYEE : MasterColumnsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int EMP_ID { get; set; }
        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string EMP_NO { get; set; }
        [StringLength(100)]
        [Required]
        public string PRE_NAME_CODE { get; set; }
        [StringLength(255)]
        [Required]
        public string FIRST_NAME { get; set; }
        [StringLength(255)]
        [Required]
        public string LAST_NAME { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        //[StringLength(3)]
        //[Required]
        //public string DEPT_CODE { get; set; }
        //[StringLength(3)]
        //[Required]
        //public string POSITION_CODE { get; set; }
    }
}
