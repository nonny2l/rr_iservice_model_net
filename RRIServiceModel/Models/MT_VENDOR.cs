﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_VENDOR : MasterColumnsModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VENDOR_ID { get; set; }
        [Required]
        public string VERDOR_NAME { get; set; }
        public int SEQ { get; set; }
        [Column(TypeName = "text")]
        public string REMARK { get; set; }
    }
}
