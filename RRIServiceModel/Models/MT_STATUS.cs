﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RRPlatFormModel.Models
{
    public partial class MT_STATUS : MasterColumnsModel
    {
        [Key]
        public long ID { get; set; }
        [StringLength(100)]
        public string STATUS_CODE { get; set; }
        [StringLength(255)]
        public string STATUS_NAME { get; set; }

        public int SEQ { get; set; }
    }
}
